#f90 = ifort
f90 = mpif90
#flags = -convert big_endian -assume byterecl -cpp
flags = -cpp

#cnv = ../physics/cumcnv.f90 
#VPATH = ../physics/

use = -Duse_cumcnv_
#obj = constants.o cumcnv.o main.o

#main : ${obj}
#	${f90} ${flags} ${obj} -o $@
obj = main.o cumcnv.o constants.o 

main : ${obj}
	${f90} ${flags} ${use} -o $@ ${obj}

main.o : main.f90 cumcnv.o constants.o 
	${f90} ${flags} ${use} -c $<

cumcnv.o : cumcnv.f90 constants.o
	${f90} ${flags} ${use} -c $<

constants.o : constants.f90 
	${f90} ${flags} ${use} -c $<

clean:
	rm -f *.o *.mod main 

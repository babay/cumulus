!--------------------------------------------------------------------
!
!     "Spectral cumulus parameterization based 
!     on cloud-resolving model"
!
!     Version 1.0 : since Jan 2014 - Dec 2016 
!
!     Developer : Yuya Baba*
!    *Application Laboratory (APL)
!     Japan Agency for Marine-Earch Science and Technology (JAMSTEC) 
!     email : babay(at)jamstec.go.jp 
! 
!     Copyright (c) 2016 Yuya Baba
!     Released under the MIT License 
!     http://opensource.org/licenses/mit-license.php
!
!--------------------------------------------------------------------
 module atmos__cumcnv

 use constants, only : DP, tmelt, elel, emelt, &
   cpair, rair, rvap, ps00, g_accel, epsvt, epsv, es0, pi

 implicit none 

 private 

 public :: cumcnv__prep
 public :: cumcnv__main

!{{{

! fixed parameter 
  integer,parameter :: &
    itl  = 2,          & 
    iti  = 3             
  real(DP),parameter ::      &
    small  = 1.0e-10_dp,     &
    tmax   = tmelt+100.0_dp, &
    tmin   = tmelt-100.0_dp, &
    p8b9   = 8.0_dp/9.0_dp,  &
    p13b20 = 13.0_dp/20.0_dp,& 
    grav   = g_accel,        &
    cp     = cpair,          &
    cpb    = 1.0_dp/cpair,   &
    cpbrair= rair/cpair,     & 
    elfm   = emelt/cpair

! variable parameter
  integer,save,private :: &
    mlt_type   = 1,       &
    wtype      = 2,       &
    det_type   = 1,       &
    evp_type   = 1,       &
    sbs_type   = 1,       &
    base_type  = 1,       &
    dnbas_type = 3,       &
    ddet_type  = 2,       &
    kh_type    = 1,       &
    sbint_type = 1
  real(DP),save,private :: &
    rkcb    = 1.0_dp,      &
    zbuo0   = 0.5_dp
  real(DP),save,private :: &
    uentmax = 4.0e-3_dp,   & 
    dentmax = 1.0e-2_dp,   &
    uturbe  = 0.5e-4_dp,   &
    uturbd  = 0.5e-4_dp,   &
    dturb   = 2.0e-4_dp,   &
    rh_fac  = 1.0_dp,      &
    wum_max = 15.0_dp,     &
    rdet    = 0.5_dp,      &
    dthv_max= 0.1_dp
  real(DP),save,private :: &
    taun   = -999.0_dp,    &
    taumin = 720.0_dp,     &
    dcapem = 0.1_dp,       &
    fxcfl  = 1.0_dp,       &
    fxcflv = 1.0_dp 
  real(DP),save,private :: &
    timin = tmelt-15.0_dp, &
    timax = tmelt,         &
    fwave
  real(DP),save,private :: &
    cpr0  = 1.85e-3_dp,    &
    cfa   = 0.15_dp,       &
    ceps  = 0.6_dp,        &
    zefld = 10.0e3_dp,     &
    wumin = 0.0_dp,        & 
    wumax = 1.4_dp
  real(DP),save,private :: &  
    cent  = 0.2_dp,        &
    a1n   = 0.5_dp,        &
    b1n   = 1.0_dp 
  real(DP),save,private :: &
    wlfs   = 1.0_dp,       &
    eta_dn = 0.3_dp,       &
    th_dn  = 1.0_dp,       &
    cconv  = 0.05_dp,      &
    adown  = 1.0_dp
  real(DP),save,private :: &
    llim_land  = 0.5e-3_dp,&
    llim_sea   = 0.3e-3_dp,&
    rhlim_land = 0.7_dp,   &
    rhlim_sea  = 0.9_dp
  logical,private,save ::  &
    use_env    = .true.,   &
    use_entorg = .true.,   &
    use_rh     = .true.,   &
    use_temp   = .false.,  &
    use_pbl    = .false.,  &
    use_clim   = .true.,   & 
    use_ice    = .true.,   &
    use_damp   = .false.,  &
    use_kalnay = .false.,  &
    use_upwnd  = .false.

  real(DP),allocatable :: wub0(:) 

  ! namelist for cloud base property 
  namelist /nmcnvcb/ & 
           rkcb, base_type, zbuo0 

  ! namelist for convective updraft 
  namelist  /nmcnvup/ceps, cfa, zefld,         &
           uentmax, uturbe, uturbd,            &
           wtype, wumax, wumin, cpr0, use_env, &
           rdet, dthv_max, det_type,           &
           rh_fac, use_rh, use_clim,           &
           wum_max, timin, use_ice, use_damp 

  ! namelist for convective adjustment closure
  namelist  /nmcnvmf/taun, &
            dcapem, fxcfl, fxcflv, use_pbl, taumin, &
            use_upwnd 

  ! namelist for new in-cloud model 
  namelist /nmcnvnew/cent, a1n, b1n, &
           llim_land, llim_sea,      &
           rhlim_land, rhlim_sea

  ! namelist for convective downdraft 
  namelist /nmcnvdn/                                & 
           mlt_type, dentmax, dturb, eta_dn, th_dn, &
           use_entorg, evp_type,                    &
           adown, dnbas_type, ddet_type, kh_type

  ! namelist for compensatory subsidence 
  namelist /nmcnvsb/use_temp, sbs_type, sbint_type, &
          use_kalnay 

!}}}
  contains 

!--------------------------------------------------------------------
!     main cumulus driver routine 
!--------------------------------------------------------------------
      subroutine cumcnv__main(              &  
           ijs, ije, kmax, ntrc, ncp, delt, &
           tt, qt, ut, vt,                  &
           prcc, snwc, prcp, snwp,          &
           ztoph, ztopl, zbas, cape,        &
           cbmfx, muent, mudet, mu, md,     &
           t, q, u, v, rdz, ro,             &
           p, z, zm, dz, dzm, lmsk )
!{{{
! output 
      real(DP),intent(out) ::     & 
           tt(ijs:ije,kmax),      & ! t tendency
           qt(ijs:ije,kmax,ntrc), & ! q tendency 
           ut(ijs:ije,kmax),      & ! u tendency
           vt(ijs:ije,kmax),      & ! v tendency
           prcc(ijs:ije),         & ! rainfall
           snwc(ijs:ije),         & ! snowfall
           ztoph(ijs:ije),        & ! cloud top height (high)
           ztopl(ijs:ije),        & ! cloud top height (low)
           zbas(ijs:ije),         & ! cloud base height 
           cape(ijs:ije),         & ! CAPE
           prcp(ijs:ije,kmax),    & ! rain flux 
           snwp(ijs:ije,kmax)       ! snow flux 
      real(DP),intent(inout) ::   & 
           muent(ijs:ije,kmax),   & ! updraft entrainment 
           mudet(ijs:ije,kmax),   & ! updraft detrainment
           cbmfx(ijs:ije,ncp),    & ! cloud base massflux 
           md(ijs:ije,kmax),      & ! downdraft mass flux
           mu(ijs:ije,kmax)         ! updraft mass flux
! input
      integer,intent(in) ::       & 
           ijs, ije, kmax,        & ! array size 
           ntrc,                  & ! number of species
           ncp                      ! number of cloud type 
      real(DP),intent(in) ::      & 
           t (ijs:ije,kmax),      & ! temperature (K) 
           q (ijs:ije,kmax,ntrc), & ! species concentration 
           u (ijs:ije,kmax),      & ! zonal velocity (m/s)
           v (ijs:ije,kmax),      & ! meridional velocity (m/s)
           p (ijs:ije,kmax),      & ! pressure (Pa)
           rdz(ijs:ije,kmax),     & ! layer thickness (rho*dz)
           ro(ijs:ije,kmax),      & ! density (kg/m3)
           delt,                  & ! time step (sec) 
           lmsk(ijs:ije)            ! land mask 
      real(DP),intent(in) ::    & 
           z  (ijs:ije,kmax  ), &   ! full level altitude (m) 
           zm (ijs:ije,kmax+1), &   ! half level altitude (m) 
           dz (ijs:ije,kmax),   &   ! full level layer thickness (m)
           dzm(ijs:ije,kmax+1)      ! half level layer thickness (m) 

#ifdef use_cumcnv_
! work arrays 
      real(DP) :: capec(ijs:ije,ncp) 

      real(DP),dimension(ijs:ije,kmax) :: & 
           qs, dqs, tv,  &
           h, s, hs, rob, rhe, &
           ptu, pqu, plu

      real(DP),dimension(ijs:ije,kmax+1) :: & 
           tvm, tm, rom, paibm, pm
      real(DP) :: qm(ijs:ije,kmax+1,ntrc)

      real(DP),dimension(ijs:ije,kmax) ::           &
           cmu, csu, cuu, cvu, cqu, ccu, ciu, cprd, &
           cmud, csud, cqud, cuud, cvud, ccud,      &
           cmuent, cmudet

      real(DP),dimension(ijs:ije) ::    & 
           cbmfxt, hc, wca, phc, dbmfx, &
           llim, rhlim, qtsum

      real(DP),dimension(ijs:ije,kmax) :: & 
           stprd, tprd, rdzb 

      real(DP),dimension(ijs:ije,kmax) :: &
           msu, mqu, mlu, miu, muu, mvu, mcu 

      real(DP),dimension(ijs:ije,kmax) ::                 & 
           mud, msud, mqud, mlud, miud, muud, mvud, mcud, &
           mdd, msdd, mqdd, mudd, mvdd,                   &
           mu_v, md_v, mud_v, mdd_v

      integer :: &
           kcb(ijs:ije), & 
           kct(ijs:ije,ncp), &
           ij, k, icp, km1, ii 
      real(DP) :: afk, abk, dum1, dum2, tfac
      real(DP) :: fice(ijs:ije) 

      qt = 0.0_dp
      tt = 0.0_dp
      ut = 0.0_dp
      vt = 0.0_dp

      ! full level values
      do k = 1, kmax
         do ij = ijs, ije 
            qs (ij,k) = fqsat(t(ij,k),p(ij,k))
            dqs(ij,k) = fdqsat(t(ij,k),qs(ij,k))
            s (ij,k) = cp*t(ij,k) + grav*z(ij,k)
            h (ij,k) = s(ij,k) + felt(t(ij,k))*q(ij,k,1)
            hs(ij,k) = s(ij,k) + felt(t(ij,k))*qs(ij,k)
            rdzb(ij,k) = 1.0_dp/rdz(ij,k) 
            if ( use_env ) then
               tv(ij,k) = t(ij,k)*(1.0_dp+epsvt*q(ij,k,1) &
                    -q(ij,k,itl)-q(ij,k,iti))
            else
               tv(ij,k) = t(ij,k)*(1.0_dp+epsvt*q(ij,k,1))
            end if 
            rob(ij,k) = 1.0_dp/ro(ij,k)
            rhe(ij,k) = q(ij,k,1)/qs(ij,k)
         end do
      end do

      ! half level values 
      do k = 1, kmax
         km1 = max(k-1,1)
         do ij = ijs, ije
            afk = dz(ij,k)/(dz(ij,k)+dz(ij,km1))
            abk = 1.0_dp - afk 
            pm(ij,k) = p(ij,k)*abk + p(ij,km1)*afk
            tm(ij,k) = t(ij,k)*abk + t(ij,km1)*afk
            rom(ij,k) = ro(ij,k)*abk + ro(ij,km1)*afk
            qm(ij,k,1) = q(ij,k,1)*abk + q(ij,km1,1)*afk
            qm(ij,k,itl) = q(ij,k,itl)*abk + q(ij,km1,itl)*afk
            qm(ij,k,iti) = q(ij,k,iti)*abk + q(ij,km1,iti)*afk
         end do
      end do
      
      do ij = ijs, ije
         pm(ij,kmax+1) = pm(ij,kmax)
         tm(ij,kmax+1) = tm(ij,kmax)
         rom(ij,kmax+1) = rom(ij,kmax)
         qm(ij,kmax+1,1) = qm(ij,kmax,1)
         qm(ij,kmax+1,itl) = qm(ij,kmax,itl)
         qm(ij,kmax+1,iti) = qm(ij,kmax,iti)
      end do

      do k = 1, kmax+1
         do ij = ijs, ije 
            if ( use_env ) then
               tvm(ij,k) = tm(ij,k)*(1.0_dp+epsvt*qm(ij,k,1) &
                    -qm(ij,k,itl)-qm(ij,k,iti))
            else
               tvm(ij,k) = tm(ij,k)*(1.0_dp+epsvt*qm(ij,k,1))
            end if 
            paibm(ij,k) = (PS00/pm(ij,k))**cpbrair
         end do
      end do

      do ij = ijs, ije
         if ( lmsk(ij) > 0.0_dp ) then
            llim (ij) = llim_land
            rhlim(ij) = rhlim_land
         else
            llim (ij) = llim_sea 
            rhlim(ij) = rhlim_sea
         end if
      end do 

!=======================================
!     cloud base property 
!=======================================

! determine cloud base height 
      call cubasetk(                &
         ijs, ije, kmax, ntrc, kcb, &
         t, p, q, z, ptu, pqu, plu ) 

! provisional cloud base mass flux
      do ij = ijs, ije
         k = kcb(ij) 
         cbmfxt(ij) = 0.1_dp*rdz(ij,k)/delt
      end do 
      
!=======================================
!     convective updraft
!=======================================

      icp_loop : do icp = 1, ncp

! updraft ent/det
         call cuascn(                       &
              ijs, ije, kmax, ntrc,         &
              kcb, kct(:,icp),              &
              hc, phc, wca, llim,           &
              cbmfxt, cmuent, cmudet,       &
              cmud, csud, cqud, cuud, cvud, &
              cmu, csu, cqu, cuu, cvu,      &
              cprd, ccu, ciu, ccud,         &
              s, tvm, rhe,                  &
              tm, q, qm, pm,                &
              u, v, z, zm, dz, rdz, paibm,  &
              ptu, pqu, plu, &
              wub0(icp) )

! convective adjustment closure
         call cuconvn(                            &
              ijs, ije, kmax, ntrc, delt,         &
              kct(:,icp), kcb,                    &
              cbmfx(:,icp), cbmfxt, capec(:,icp), &
              hc, wca,                            &
              cmu, csu, cqu, ccu,                 & 
              t, tv, tvm, s, q,                   &
              z, dz, rdz, dzm )

! bulk mass flux, precipi, condensate 
         call cuflxn(                                  &
              ijs, ije, kmax, ncp, icp,                &
              cbmfx, cbmfxt,                           &
              cprd, cmu, csu, cqu, ccu, ciu, cuu, cvu, &
              cmud, csud, cqud, ccud, cuud, cvud,      &
              cmuent, cmudet,                          &
              tprd, mu, msu, mqu, mcu, miu, muu, mvu,  &
              mud, msud, mqud, mcud, muud, mvud,       &
              muent, mudet ) 
         
     end do icp_loop 

! updraft mass flux limiter
     call culimup(                        &
          ijs, ije, kmax, ncp, kcb, delt, &
          dz, rdz,                        & 
          mu, mu_v, mud, mud_v,           &
          msud, mqud, mcud, muud, mvud,   &
          tprd, dbmfx )

! liquid/ice partition 
     call cupart(          &
          ijs, ije, kmax,  &
          t, mu, msu,      &
          mcu, mcud, tprd, & 
          mlu, miu, mlud, miud, stprd ) 
     
     cape = 0.0_dp 
     do ii = 1, ncp 
        do ij = ijs, ije
           k = kct(ij,ii)
           cape(ij) = cape(ij) + capec(ij,ii) 
        end do
     end do

     zbas  = 0.0_dp 
     ztoph = 0.0_dp
     ztopl = 0.0_dp 
     do ij = ijs, ije
        zbas(ij) = z(ij,kcb(ij)) 
        k = kct(ij,1)
        if ( k > 0 ) ztoph(ij) = max(ztoph(ij),z(ij,k))
        k = kct(ij,ncp)
        if ( k > 0 ) ztopl(ij) = max(ztopl(ij),z(ij,k))
     end do 

!=======================================
!     convective downdraft
!=======================================

! downdraft ent/det 
     call cudscn(                              & 
          ijs, ije, kmax, ntrc, kcb, delt,     &
          tt, qt,                              &
          prcp, snwp, prcc, snwc,              &
          mu, md, mdd, msdd, mqdd, mudd, mvdd, &
          tprd, stprd,                         &
          s, q, u, v,                          &
          qs, hs, h, t, tv, p, pm, ro,         &
          msu, mqu, muu, mvu,                  &
          z, zm, dqs,                          &
          dz, rdz, rdzb, rhlim, dbmfx ) 

!=======================================
!     detrainment 
!=======================================

! calc. total detrainment tendency 
     call cudetall(                           &
          ijs, ije, kmax, ntrc, kcb, delt,    &
          tt, qt, ut, vt,                     &
          dbmfx, md, mdd, md_v, mdd_v,        &
          msdd, mqdd, mudd, mvdd,             &
          mud, mud_v,                         &
          msud, mqud, mlud, miud, muud, mvud, &
          msu, mqu, muu, mvu,                 &
          s, q, u, v, rob,                    &
          zm, dz, rdz, rdzb  ) 
     
!=======================================
!     subsidence & tendencies to env.
!=======================================

     if ( use_kalnay ) then
        tfac = 2.0_dp 
     else
        tfac = 1.0_dp
     end if

     if ( sbs_type == 1 ) then
        call cusbsdn(              &
             ijs, ije, kmax, ntrc, &
             tfac*delt,            &
             tt, qt, ut, vt,       &
             s, q, u, v, t,        &
             rob, dzm,             &
             mu, md, mu_v, md_v )
     else
        call cusbsdn_div(          &
             ijs, ije, kmax, ntrc, &
             tfac*delt,            &
             tt, qt, ut, vt,       &
             s, q, u, v, t,        &
             ro, rob, dzm,         &
             mu, md, mu_v, md_v )
     end if

#endif 
!}}}
      end subroutine cumcnv__main

#ifdef use_cumcnv_

!--------------------------------------------------------------------
!     definition of cloud base (based on TK89) 
!--------------------------------------------------------------------
      subroutine cubasetk(          &
         ijs, ije, kmax, ntrc, kcb, &
         t, p, q, z, tup, qup, lup ) 
!{{{
! output 
      integer,intent(out) :: &
        kcb(ijs:ije) 
      real(DP),intent(out) ::  &
        tup(ijs:ije,kmax), &
        qup(ijs:ije,kmax), &
        lup(ijs:ije,kmax)
! input
      integer,intent(in) :: &
        ijs, ije, kmax, ntrc
      real(DP),intent(in) ::  &
        t(ijs:ije,kmax),      &
        p(ijs:ije,kmax),      &
        q(ijs:ije,kmax,ntrc), &
        z(ijs:ije,kmax)
! work
      integer :: klab(ijs:ije,kmax)
      integer :: ij, k 
      logical :: llo(ijs:ije) 
      real(DP) :: ccnd(ijs:ije) 
      real(DP) :: gam, qsat, zbuo 
        
      do k = 1 ,kmax
        do ij = ijs, ije
          klab(ij,k) = 0
          qup(ij,k) = q(ij,k,1) 
          tup(ij,k) = t(ij,k) 
          lup(ij,k) = 0.0_dp 
        end do 
      end do 
      
      k = 1 
      do ij = ijs, ije
         klab(ij,k) = 1 
         kcb(ij) = kmax
      end do 

      kloop : do k = 2, kmax

         do ij = ijs, ije 
            if ( klab(ij,k-1) == 1 ) then
               llo(ij) = .true.
            else
               llo(ij) = .false.
            end if
         end do
         
         do ij = ijs, ije
            if ( llo(ij) ) then
               qup(ij,k) = qup(ij,k-1) 
               tup(ij,k) = (cp*tup(ij,k-1)+grav*z(ij,k-1) &
                    -grav*z(ij,k))*cpb
               lup(ij,k) = lup(ij,k-1) 
               ! dry adiabatic buoyancy check 
               zbuo = tup(ij,k)*(1.0_dp+epsvt*qup(ij,k)) &
                    -t(ij,k)*(1.0_dp+epsvt*q(ij,k,1)) + zbuo0 
               if ( zbuo > 0.0_dp ) klab(ij,k) = 1 
               ! calc moisture adjustment 
               qsat = fqsat(tup(ij,k),p(ij,k))
               gam = felt(tup(ij,k))*cpb*fdqsat(tup(ij,k),qsat) 
               ccnd(ij) = max(qup(ij,k)-qsat,0.0_dp)/(1.0_dp+gam)
               qup(ij,k) = qup(ij,k) - ccnd(ij) 
               tup(ij,k) = tup(ij,k) + ccnd(ij)*felt(tup(ij,k))*cpb
               lup(ij,k) = lup(ij,k) + ccnd(ij) 
            else
               ccnd(ij) = 0.0_dp
            end if
         end do

         do ij = ijs, ije
            if ( llo(ij) .and. ccnd(ij)>0.0_dp ) then
               klab(ij,k) = 2 
               ! moist adiabatic buoyancy check
               zbuo = tup(ij,k)*(1.0_dp+epsvt*qup(ij,k)-lup(ij,k)) &
                    -t(ij,k)*(1.0_dp+epsvt*q(ij,k,1)) + zbuo0 
               if ( zbuo > 0.0_dp ) then 
                  klab(ij,k) = 2
                  kcb(ij) = k
               end if
            end if
         end do

      end do kloop 

!}}}
      end subroutine cubasetk

!--------------------------------------------------------------------
!     updraft entrainment/detrainment
!     note: momentum * massflux can be negative value.
!--------------------------------------------------------------------
      subroutine cuascn(                 & 
           ijs, ije, kmax, ntrc,         & 
           kcb, kct,                     & 
           hc, phc, wca, llim,           &
           cbmfxt, cmuent, cmudet,       & 
           cmud, csud, cqud, cuud, cvud, & 
           cmu, csu, cqu, cuu, cvu,      &
           cprd, ccu, ciu, ccud,         & 
           s, tvm, rhe,                  &
           tm, q, qm, pm,                &
           u, v, z, zm, dz, rdz, paibm,  & 
           ptu, pqu, plu, &
           wub )
!{{{

! output 
      real(DP),intent(out) ::   &
           cmuent(ijs:ije,kmax),&
           cmudet(ijs:ije,kmax),&
           cprd(ijs:ije,kmax) 
      real(DP),intent(out) ::  &
           cmud(ijs:ije,kmax), &
           csud(ijs:ije,kmax), &
           cqud(ijs:ije,kmax), &
           cuud(ijs:ije,kmax), &
           cvud(ijs:ije,kmax), &
           ccud(ijs:ije,kmax) 
! inout
      real(DP),intent(inout) :: &
           csu(ijs:ije,kmax),   & 
           cqu(ijs:ije,kmax),   &   
           cuu(ijs:ije,kmax),   & 
           cvu(ijs:ije,kmax),   & 
           cmu(ijs:ije,kmax),   &
           ccu(ijs:ije,kmax),   &
           ciu(ijs:ije,kmax)
      real(DP),intent(inout) :: &
           hc(ijs:ije),         &
           phc(ijs:ije),        &
           wca(ijs:ije)
      integer,intent(inout) :: &
           kct(ijs:ije)
! input 
      integer,intent(in) ::      &
           kmax, ijs, ije, ntrc, &
           kcb(ijs:ije) 
      real(DP),intent(in) ::        &
           cbmfxt(ijs:ije),         &
           llim(ijs:ije),           &
           s(ijs:ije,kmax),         &
           tvm(ijs:ije,kmax+1),     &
           rhe(ijs:ije,kmax),       &
           q(ijs:ije,kmax,ntrc),    &
           qm(ijs:ije,kmax+1,ntrc), &
           tm(ijs:ije,kmax+1),      &
           pm(ijs:ije,kmax+1)
      real(DP),intent(in) ::      &
           u(ijs:ije,kmax),       &
           v(ijs:ije,kmax),       &
           paibm(ijs:ije,kmax+1), & 
           wub 
      real(DP),intent(in) ::   & 
           z(ijs:ije,kmax),    &
           zm(ijs:ije,kmax+1), &
           dz(ijs:ije,kmax),   &
           rdz(ijs:ije,kmax), &
           ptu(ijs:ije,kmax), &
           pqu(ijs:ije,kmax), &
           plu(ijs:ije,kmax)
! work 
      real(DP) :: wum(ijs:ije,kmax+1) 

      real(DP),dimension(ijs:ije,kmax+1) :: &
         cmum, csum, cqum, ccum, cuum, cvum
      real(DP),dimension(ijs:ije) :: &
         scb, qcb, ucb, vcb, lcb, icb

      real(DP) :: cium(ijs:ije,kmax+1) 
      real(DP) :: ciud(ijs:ije,kmax) 
      real(DP) :: ciprd(ijs:ije,kmax) 

      real(DP) :: entr(ijs:ije,kmax)
      real(DP) :: detr(ijs:ije,kmax)
      real(DP) :: entrm(ijs:ije,kmax+1) 
      real(DP) :: detrm(ijs:ije,kmax+1) 
      real(DP) :: entt(ijs:ije,kmax)
      real(DP) :: dett(ijs:ije,kmax)

      real(DP) :: buoy(ijs:ije,kmax)    
      real(DP) :: buoym(ijs:ije,kmax+1) 

      real(DP) :: dthvm(ijs:ije,kmax+1) 

      logical :: cmflg(ijs:ije) 

      integer :: ij, k, kp
           
      real(DP) :: dum1, dum2, dum3, &           
           entdz, detdz, ccnd, &
           entrt, detrt, tupm, &
           lupm, supm, qupm, &
           qsat, dthvdz, gam, tvupm, fice, fice0

! in-cloud values*eta (full) 
      cmu(:,:) = 0.0_dp
      csu(:,:) = 0.0_dp
      cuu(:,:) = 0.0_dp
      cvu(:,:) = 0.0_dp
      cqu(:,:) = 0.0_dp
      ccu(:,:) = 0.0_dp
      cprd(:,:) = 0.0_dp

! in-cloud values*eta (half)
      cmum(:,:) = 0.0_dp 
      csum(:,:) = 0.0_dp
      cuum(:,:) = 0.0_dp
      cvum(:,:) = 0.0_dp
      cqum(:,:) = 0.0_dp
      ccum(:,:) = 0.0_dp

      ciu = 0.0_dp
      cium = 0.0_dp
      ciud = 0.0_dp
      ciprd = 0.0_dp

      buoy (:,:) = 0.0_dp 
      buoym(:,:) = 0.0_dp
      dthvm(:,:) = 0.0_dp 
      wum  (:,:) = 0.0_dp

      detr(:,:) = 0.0_dp
      entr(:,:) = 0.0_dp
      entrm(:,:) = 0.0_dp 
      detrm(:,:) = 0.0_dp 
      cmuent(:,:) = 0.0_dp
      cmudet(:,:) = 0.0_dp

! detrainment terms
      cmud(:,:) = 0.0_dp
      csud(:,:) = 0.0_dp
      cqud(:,:) = 0.0_dp
      cuud(:,:) = 0.0_dp
      cvud(:,:) = 0.0_dp
      ccud(:,:) = 0.0_dp

      cmflg(:) = .true. 

      ! turbulent ent/det 
      do k = 1, kmax
         do ij = ijs, ije
            if ( use_rh ) then
               entt(ij,k) = uturbe*max(rh_fac-rhe(ij,k),0.0_dp)
               dett(ij,k) = uturbd*max(rh_fac-rhe(ij,k),0.0_dp)
            else
               entt(ij,k) = uturbe
               dett(ij,k) = uturbd
            end if 
         end do
      end do

      ! cloud base properties 
      if ( base_type == 1 ) then
         do ij = ijs, ije
            k = kcb(ij) 
            scb(ij) = cp*ptu(ij,k) + grav*z(ij,k)
            qcb(ij) = pqu(ij,k)
            ucb(ij) = u(ij,k)
            vcb(ij) = v(ij,k)
            lcb(ij) = plu(ij,k)
            icb(ij) = plu(ij,k)*frcice(ptu(ij,k))
         end do 
         do k = 1, kmax 
            do ij = ijs, ije 
               if ( k <= kcb(ij) ) then
                  cmum(ij,k) = cbmfxt(ij) &
                       *(zm(ij,k)-zm(ij,1)) &
                       /(zm(ij,kcb(ij))-zm(ij,1))
                  csum(ij,k) = scb(ij)*cmum(ij,k)
                  cqum(ij,k) = qcb(ij)*cmum(ij,k)
                  cuum(ij,k) = ucb(ij)*cmum(ij,k) 
                  cvum(ij,k) = vcb(ij)*cmum(ij,k) 
                  ccum(ij,k) = lcb(ij)*cmum(ij,k)
                  cium(ij,k) = icb(ij)*cmum(ij,k) 
                  if ( k < kcb(ij) ) then
                  ccum(ij,k) = 0.0_dp
                  cium(ij,k) = 0.0_dp
                  end if 
               end if
            end do
         end do
      else 
         do k = 1, kmax 
            do ij = ijs, ije 
               if ( k <= kcb(ij) ) then
                  cmum(ij,k) = cbmfxt(ij) &
                       *( (zm(ij,k)-zm(ij,1)) &
                       /(zm(ij,kcb(ij))-zm(ij,1)) )**rkcb
               end if
            end do
         end do
         do k = 1, kmax 
            do ij = ijs, ije 
               if ( k < kcb(ij) ) then
                  entdz = cmum(ij,k+1)-cmum(ij,k)
                  csum(ij,k+1) = csum(ij,k) + entdz*s(ij,k)
                  cqum(ij,k+1) = cqum(ij,k) + entdz*q(ij,k,1)
                  cuum(ij,k+1) = cuum(ij,k) + entdz*u(ij,k)
                  cvum(ij,k+1) = cvum(ij,k) + entdz*v(ij,k)
                  if ( use_env ) then
                     ccum(ij,k+1) = ccum(ij,k) &
                          + entdz*(q(ij,k,itl)+q(ij,k,iti))
                     cium(ij,k+1) = cium(ij,k) &
                          + entdz*q(ij,k,iti)
                  end if
               end if
            end do
         end do
      end if 

      ! subcloud full level values 
      do k = 1, kmax 
         do ij = ijs, ije 
            if ( k < kcb(ij) ) then
               cmu(ij,k) = 0.5_dp*(cmum(ij,k+1)+cmum(ij,k))
               csu(ij,k) = 0.5_dp*(csum(ij,k+1)+csum(ij,k))
               cqu(ij,k) = 0.5_dp*(cqum(ij,k+1)+cqum(ij,k))
               cuu(ij,k) = 0.5_dp*(cuum(ij,k+1)+cuum(ij,k))
               cvu(ij,k) = 0.5_dp*(cvum(ij,k+1)+cvum(ij,k))
               ccu(ij,k) = 0.5_dp*(ccum(ij,k+1)+ccum(ij,k))
               ciu(ij,k) = 0.5_dp*(cium(ij,k+1)+cium(ij,k))
            end if
         end do
      end do

      !============================
      ! set cloud base properties 
      !============================
      do ij = ijs, ije 

        k = kcb(ij) 
        wum(ij,k) = wub

        ! dse correction
        tupm = (csum(ij,k)/cmum(ij,k)-grav*zm(ij,k))*cpb
        tupm = min(max(tupm,tmin),tmax)
        csum(ij,k) = (cp*tupm+grav*zm(ij,k))*cmum(ij,k)

        ! condensation 
        qsat = fqsat(tupm,pm(ij,k))
        gam = felt(tupm)*cpb*fdqsat(tupm,qsat) 

        ccnd = (cqum(ij,k)-qsat*cmum(ij,k))/(1.0_dp+gam)
        ccnd = min(max(ccnd,0.0_dp),cqum(ij,k))

        cqum(ij,k) = cqum(ij,k) - ccnd
        ccum(ij,k) = ccum(ij,k) + ccnd
        csum(ij,k) = csum(ij,k) + ccnd*felt(tupm)
        cium(ij,k) = cium(ij,k) + ccnd*frcice(tm(ij,k))

        ! freezing
        if ( use_ice ) then
          fice = min(cium(ij,k)/max(ccum(ij,k),small),1.0_dp)
          fice0 = frcice(tm(ij,k))
          if ( fice < fice0 ) then
            dum1 = fice0*ccum(ij,k)-cium(ij,k)
            dum1 = max(min(dum1,ccum(ij,k)-cium(ij,k)),0.0_dp)
          else
            dum1 = 0.0_dp
          end if
          cium(ij,k) = cium(ij,k) + dum1
          csum(ij,k) = csum(ij,k) + dum1*emelt
        end if

        ! cloud base buoyancy 
        supm = csum(ij,k)/cmum(ij,k) 
        qupm = cqum(ij,k)/cmum(ij,k)
        lupm = ccum(ij,k)/cmum(ij,k)
        tupm = cpb*(supm-grav*zm(ij,k))
        tvupm = tupm*(1.0_dp+epsvt*qupm-lupm)

        buoym(ij,k) = grav*(tvupm-tvm(ij,k))/tvm(ij,k)

        dthvm(ij,k) = (tvupm-tvm(ij,k))*paibm(ij,k)
        dthvm(ij,k) = max(dthvm(ij,k),0.0_dp)
        
        ! cloud base entrainment rate
        entrm(ij,k) = 0.0_dp
        detrm(ij,k) = 0.0_dp

        if ( wtype == 1 ) then
           entrm(ij,k) = ceps*cfa*max(buoym(ij,k),0.0_dp)/wub
        else
           entrm(ij,k) = cent*max(buoym(ij,k),0.0_dp)/wub
        end if
        entrm(ij,k) = max(min(entrm(ij,k),uentmax),0.0_dp)

      end do

      !===================================
      ! vertical integration for updraft 
      !===================================
      kloop : do k = 1, kmax
         kp = min( k+1, kmax )

         do ij = ijs, ije ! entrain 
            if ( k>=kcb(ij) .and. cmflg(ij) ) then
               
               entrt = entrm(ij,k) + entt(ij,k)
               entdz = dz(ij,k)*entrt*cmum(ij,k)

               ! precipi 
               lupm = ccum(ij,k)/cmum(ij,k)
               supm = csum(ij,k)/cmum(ij,k)
               tupm = cpb*(supm-grav*zm(ij,k))
               dum1 = max(sqrt(wum(ij,k)),small)
               cprd(ij,k) = fprc(lupm,llim(ij),dum1,tupm) &
                  *dz(ij,k)*ccum(ij,k)
               cprd(ij,k) = min(cprd(ij,k),ccum(ij,k))

               ! ice precipi
               fice = min(cium(ij,k)/max(ccum(ij,k),small),1.0_dp)
               ciprd(ij,k) = min(fice*cprd(ij,k),cium(ij,k))

               cmum(ij,kp) = cmum(ij,k) + entdz
               csum(ij,kp) = csum(ij,k) + entdz*s(ij,k)
               cqum(ij,kp) = cqum(ij,k) + entdz*q(ij,k,1)
               if ( use_env ) then
                  ccum(ij,kp) = ccum(ij,k) &
                       + entdz*(q(ij,k,itl)+q(ij,k,iti)) - cprd(ij,k)
                  cium(ij,kp) = cium(ij,k) &
                       + entdz*q(ij,k,iti) - ciprd(ij,k)
               else
                  ccum(ij,kp) = ccum(ij,k) - cprd(ij,k)
                  cium(ij,kp) = cium(ij,k) - ciprd(ij,k)
               end if 
               cium(ij,kp) = min(cium(ij,kp),ccum(ij,kp))

               cmum(ij,kp) = max(cmum(ij,kp),0.0_dp)
               csum(ij,kp) = max(csum(ij,kp),0.0_dp)
               cqum(ij,kp) = max(cqum(ij,kp),0.0_dp)
               ccum(ij,kp) = max(ccum(ij,kp),0.0_dp)
               cium(ij,kp) = max(cium(ij,kp),0.0_dp)

               if ( cmum(ij,kp)<small ) then
                  wum(ij,kp) = 0.0_dp 
                  cmflg(ij) = .false. 
               end if

            end if
         end do

         do ij = ijs, ije ! physics 
            if ( k>=kcb(ij) .and. cmflg(ij) ) then
               
               ! dse correction 
               tupm = (csum(ij,kp)/cmum(ij,kp)-grav*zm(ij,kp))*cpb 
               tupm = min(max(tupm,tmin),tmax)
               csum(ij,kp) = (cp*tupm+grav*zm(ij,kp))*cmum(ij,kp) 
               
               ! condensation
               qsat = fqsat(tupm,pm(ij,kp))
               gam = felt(tupm)*cpb*fdqsat(tupm,qsat) 
               
               ccnd = (cqum(ij,kp)-qsat*cmum(ij,kp))/(1.0_dp+gam)
               ccnd = min(max(ccnd,0.0_dp),cqum(ij,kp))
               
               cqum(ij,kp) = cqum(ij,kp) - ccnd 
               ccum(ij,kp) = ccum(ij,kp) + ccnd 
               csum(ij,kp) = csum(ij,kp) + ccnd*felt(tupm)
               cium(ij,kp) = cium(ij,kp) + ccnd*frcice(tm(ij,kp))

               ! freezing
               if ( use_ice ) then
                  fice = min(cium(ij,kp)/max(ccum(ij,kp),small),1.0_dp)
                  fice0 = frcice(tm(ij,kp))
                  if ( fice < fice0 ) then
                     dum1 = fice0*ccum(ij,kp)-cium(ij,kp)
                     dum1 = max(min(dum1,ccum(ij,kp)-cium(ij,kp)),0.0_dp)
                  else
                     dum1 = 0.0_dp
                  end if
                  cium(ij,kp) = cium(ij,kp) + dum1
                  csum(ij,kp) = csum(ij,kp) + dum1*emelt
               end if
               
               ! upper half level buoyancy 
               supm = csum(ij,kp)/cmum(ij,kp) 
               qupm = cqum(ij,kp)/cmum(ij,kp) 
               lupm = ccum(ij,kp)/cmum(ij,kp) 
               tupm = cpb*(supm-grav*zm(ij,kp))
               tvupm = tupm*(1.0_dp+epsvt*qupm-lupm)

               buoym(ij,kp) = grav*(tvupm-tvm(ij,kp))/tvm(ij,kp)
               
               buoy(ij,k)= 0.5_dp*(buoym(ij,kp)+buoym(ij,k))

               dthvm(ij,kp) = (tvupm-tvm(ij,kp))*paibm(ij,kp)
               dthvm(ij,kp) = max(dthvm(ij,kp),0.0_dp)
            end if
         end do

         do ij = ijs, ije ! detrain 
            if ( k>=kcb(ij) .and. cmflg(ij) ) then

               ! detrainment rate
               if ( det_type > 0 ) then
                  !dthv = max(0.5_dp*(dthvm(ij,k)+dthvm(ij,kp)),small)
                  dum1 = max(0.5_dp*(dthvm(ij,k)+dthvm(ij,kp)),small)
                  dthvdz = min(max((dthvm(ij,k)-dthvm(ij,kp))/dum1,0.0_dp), &
                    dthv_max)/dz(ij,k)
                  !detr(ij,k) = -rdet*(dthvm(ij,kp)-dthvm(ij,k))/(dthv*dz(ij,k))
                  !detr(ij,k) = -rdet*dthvdz
                  detr(ij,k) = rdet*dthvdz
               else
                  detr(ij,k) = 0.0_dp
               end if
               detr(ij,k) = max(min(detr(ij,k),1.0_dp/dz(ij,k)),0.0_dp)
               detrm(ij,kp) = max(2.0_dp*detr(ij,k)-detrm(ij,k),0.0_dp)

               detrt = detr(ij,k) + dett(ij,k) 
               detrt = min(detrt,1.0_dp/dz(ij,k))
               detdz = dz(ij,k)*detrt
               
               cmud(ij,k) = cmum(ij,kp)*detdz
               csud(ij,k) = csum(ij,kp)*detdz
               cqud(ij,k) = cqum(ij,kp)*detdz
               ccud(ij,k) = ccum(ij,kp)*detdz
               ciud(ij,k) = cium(ij,kp)*detdz
               
               cmum(ij,kp) = cmum(ij,kp)-cmud(ij,k)
               csum(ij,kp) = csum(ij,kp)-csud(ij,k)
               cqum(ij,kp) = cqum(ij,kp)-cqud(ij,k)
               ccum(ij,kp) = ccum(ij,kp)-ccud(ij,k)
               cium(ij,kp) = cium(ij,kp)-ciud(ij,k)

               cmum(ij,kp) = max(cmum(ij,kp),0.0_dp)
               csum(ij,kp) = max(csum(ij,kp),0.0_dp)
               cqum(ij,kp) = max(cqum(ij,kp),0.0_dp)
               ccum(ij,kp) = max(ccum(ij,kp),0.0_dp)
               cium(ij,kp) = max(cium(ij,kp),0.0_dp)

               cmu(ij,k) = 0.5_dp*(cmum(ij,kp)+cmum(ij,k))
               csu(ij,k) = 0.5_dp*(csum(ij,kp)+csum(ij,k))
               cqu(ij,k) = 0.5_dp*(cqum(ij,kp)+cqum(ij,k))
               ccu(ij,k) = 0.5_dp*(ccum(ij,kp)+ccum(ij,k))
               ciu(ij,k) = 0.5_dp*(cium(ij,kp)+cium(ij,k))

               ! recalc upper half buoyancy 
               if ( cmum(ij,kp) > 0.0_dp ) then
                  supm = csum(ij,kp)/cmum(ij,kp) 
                  qupm = cqum(ij,kp)/cmum(ij,kp) 
                  lupm = ccum(ij,kp)/cmum(ij,kp) 
                  tupm = cpb*(supm-grav*zm(ij,kp))
                  tvupm = tupm*(1.0_dp+epsvt*qupm-lupm)
                  buoym(ij,kp) = grav*(tvupm-tvm(ij,kp))/tvm(ij,kp)
               else
                  buoym(ij,kp) = 0.0_dp 
               end if
               buoy(ij,k)= 0.5_dp*(buoym(ij,kp)+buoym(ij,k))

               if ( cmum(ij,kp)<small ) then
                  wum(ij,kp) = 0.0_dp 
                  cmflg(ij) = .false. 
               end if

            end if
         end do

         do ij = ijs, ije ! updraft velocity 
            if ( k>=kcb(ij) .and. cmflg(ij) ) then

               ! update updraft velocity 
               if ( wtype == 1 ) then
                  !dum1 = 0.5_dp/dz(ij,k)
                  dum1 = 0.5_dp/dz(ij,k) + 0.5_dp/zefld
                  dum2 = 0.5_dp/dz(ij,k)*wum(ij,k) + cfa*(1.0_dp-ceps)*buoy(ij,k) 
                  !if ( use_damp ) dum1 = dum1 + 0.5_dp/zefld
                  wum(ij,kp) = dum2/dum1 
               else
                  dum1 = 0.5_dp/dz(ij,k) + 0.5_dp*(b1n*detr(ij,k)) 
                  if ( use_damp ) dum1 = dum1 + 0.5_dp/zefld
                  dum2 = 0.5_dp/dz(ij,k)*wum(ij,k) + a1n*buoy(ij,k) &
                       - 0.5_dp*(b1n*detr(ij,k))*wum(ij,k)
                  wum(ij,kp) = dum2/dum1 
               end if

               ! limit updraft velocity 
               wum(ij,kp) = min(max(wum(ij,kp),0.0_dp),wum_max**2)

               ! half level entrainment rate
               entrm(ij,kp) = 0.0_dp
               if ( wtype == 1 ) then
                  entrm(ij,kp) = ceps*cfa*buoym(ij,kp)/max(wum(ij,kp),small)
               else
                  entrm(ij,kp) = cent*(buoym(ij,kp)-detrm(ij,kp)*wum(ij,kp)) &
                    /max(wum(ij,kp),small)
               end if
               
               ! full level entrainment rate 
               entrm(ij,kp) = max(min(entrm(ij,kp),uentmax),0.0_dp)
               entr(ij,k) = 0.5_dp*(entrm(ij,k)+entrm(ij,kp)) 
               
               if ( wum(ij,kp)<=0.0_dp ) then
                  cmflg(ij) = .false. 
               end if
                  
            end if
         end do
         
      end do kloop

      ! set cloud top index
      kct(:) = -1
      do k = kmax, 2, -1
         do ij = ijs, ije
            if ( k>kcb(ij) .and. kct(ij)<0 .and. &
               wum(ij,k)>0.0_dp ) then
               kct(ij) = k 
            end if
         end do
      end do

      do ij = ijs, ije
         kct(ij) = min(kct(ij),kmax-1) 
      end do

      ! depth of convection 
      hc (:) = 0.0_dp 
      phc(:) = 0.0_dp 
      wca(:) = 0.0_dp 
      wum = max(wum,0.0_dp) 
      do k = 1, kmax
         do ij = ijs, ije
            if ( k>=kcb(ij) .and. k<=kct(ij) ) then
               hc (ij) = hc (ij) + dz(ij,k) 
               phc(ij) = phc(ij) + grav*rdz(ij,k) 
               wca(ij) = wca(ij) + dz(ij,k) &
                    *sqrt(0.5_dp*(wum(ij,k+1)+wum(ij,k)))
            end if 
         end do 
      end do 

      ! convective momentum transport 
      do k = 1, kmax
         kp = k + 1 
         do ij = ijs, ije
            if ( k>=kcb(ij) .and. k<=kct(ij) ) then

               ! momentum ent/det 
               entrt = entrm(ij,k) + entt(ij,k) 
               detrt = detr (ij,k) + dett(ij,k)
               detrt = min(detrt,1.0_dp/dz(ij,k))

               entdz = dz(ij,k)*entrt*cmum(ij,k)
               detdz = dz(ij,k)*detrt

               ! entrainment
               cuum(ij,kp) = cuum(ij,k) + entdz*u(ij,k)
               cvum(ij,kp) = cvum(ij,k) + entdz*v(ij,k)

               ! detrainment
               cuud(ij,k) = cuum(ij,kp)*detdz
               cvud(ij,k) = cvum(ij,kp)*detdz

               cuum(ij,kp) = cuum(ij,kp)-cuud(ij,k)
               cvum(ij,kp) = cvum(ij,kp)-cvud(ij,k)

               ! full level value 
               cuu(ij,k) = 0.5_dp*(cuum(ij,kp)+cuum(ij,k))
               cvu(ij,k) = 0.5_dp*(cvum(ij,kp)+cvum(ij,k))
            end if
         end do
      end do

      ! entrainment/detrainment 
      do k = 1, kmax
        do ij = ijs, ije
          if ( k>=kcb(ij).and.k<=kct(ij) ) then
             entrt = entrm(ij,k) + entt(ij,k) 
             !detrt = detr (ij,k) + dett(ij,k)
             cmuent(ij,k) = entrt*cmum(ij,k)
             cmudet(ij,k) = cmud(ij,k)/dz(ij,k)
          end if 
        end do 
      end do 

      ! reset cloud top values
      do k = 1, kmax
         do ij = ijs, ije
            if ( k >= kct(ij) ) then
               ! in-cloud values
               cmu(ij,k) = 0.0_dp
               csu(ij,k) = 0.0_dp
               cqu(ij,k) = 0.0_dp
               ccu(ij,k) = 0.0_dp
               ciu(ij,k) = 0.0_dp
               cuu(ij,k) = 0.0_dp
               cvu(ij,k) = 0.0_dp
               ! precipi production
               cprd (ij,k) = 0.0_dp
               ciprd(ij,k) = 0.0_dp
               ! detrainment terms
               cmud(ij,k) = 0.0_dp
               csud(ij,k) = 0.0_dp
               cqud(ij,k) = 0.0_dp
               ccud(ij,k) = 0.0_dp
               ciud(ij,k) = 0.0_dp
               cuud(ij,k) = 0.0_dp
               cvud(ij,k) = 0.0_dp
               buoy(ij,k) = 0.0_dp 
            end if
         end do 
      end do 

      ! recalculate cloud top properties 
      do ij = ijs, ije 
         if ( kct(ij) > 0 ) then

            ! cloud top 
            k  = kct(ij) 
            kp = kct(ij)+1

            ! cloud top precipi
            lupm = ccum(ij,k)/cmum(ij,k)
            supm = csum(ij,k)/cmum(ij,k) 
            tupm = cpb*(supm-grav*zm(ij,k))
            dum1 = max(sqrt(wum(ij,k)),small)
            cprd(ij,k) = fprc(lupm,llim(ij),dum1,tupm) &
                 *dz(ij,k)*ccum(ij,k)
            cprd(ij,k) = min(cprd(ij,k),ccum(ij,k))

            ! ice precipi
            fice = min(cium(ij,k)/max(ccum(ij,k),small),1.0_dp)
            ciprd(ij,k) = min(fice*cprd(ij,k),cium(ij,k))

            entrt = entrm(ij,k) + entt(ij,k) 
            entdz = dz(ij,k)*entrt*cmum(ij,k)

            cmum(ij,kp) = cmum(ij,k) + entdz
            csum(ij,kp) = csum(ij,k) + entdz*s(ij,k)
            cqum(ij,kp) = cqum(ij,k) + entdz*q(ij,k,1) 

            if ( use_env ) then
               ccum(ij,kp) = ccum(ij,k) &
                    + entdz*(q(ij,k,itl)+q(ij,k,iti)) - cprd(ij,k) 
               cium(ij,kp) = cium(ij,k) &
                    + entdz*q(ij,k,iti) - ciprd(ij,k)
            else
               ccum(ij,kp) = ccum(ij,k) - cprd(ij,k) 
               cium(ij,kp) = cium(ij,k) - ciprd(ij,k) 
            end if 
            cium(ij,kp) = min(cium(ij,kp),ccum(ij,kp))

            cuum(ij,kp) = cuum(ij,k) + entdz*u(ij,k)
            cvum(ij,kp) = cvum(ij,k) + entdz*v(ij,k) 

            ! detrainment 
            cmud(ij,k) = cmum(ij,kp)
            csud(ij,k) = csum(ij,kp)
            cqud(ij,k) = cqum(ij,kp)
            ccud(ij,k) = ccum(ij,kp)
            ciud(ij,k) = cium(ij,kp)
            cuud(ij,k) = cuum(ij,kp)
            cvud(ij,k) = cvum(ij,kp)

            ! decrease by detrain 
            cmum(ij,kp) = cmum(ij,kp)-cmud(ij,k)
            csum(ij,kp) = csum(ij,kp)-csud(ij,k)
            cqum(ij,kp) = cqum(ij,kp)-cqud(ij,k)
            ccum(ij,kp) = ccum(ij,kp)-ccud(ij,k)
            cium(ij,kp) = cium(ij,kp)-ciud(ij,k)
            cuum(ij,kp) = cuum(ij,kp)-cuud(ij,k)
            cvum(ij,kp) = cvum(ij,kp)-cvud(ij,k)

            ! cloud top values
            cmu(ij,k) = 0.5_dp*(cmum(ij,kp)+cmum(ij,k))
            csu(ij,k) = 0.5_dp*(csum(ij,kp)+csum(ij,k))
            cqu(ij,k) = 0.5_dp*(cqum(ij,kp)+cqum(ij,k))
            ccu(ij,k) = 0.5_dp*(ccum(ij,kp)+ccum(ij,k))
            ciu(ij,k) = 0.5_dp*(cium(ij,kp)+cium(ij,k))
            cuu(ij,k) = 0.5_dp*(cuum(ij,kp)+cuum(ij,k))
            cvu(ij,k) = 0.5_dp*(cvum(ij,kp)+cvum(ij,k))
         end if
      end do

!}}}
      end subroutine cuascn

!--------------------------------------------------------------------
!     convective adjustment closure
!--------------------------------------------------------------------
      subroutine cuconvn(              &           
           ijs, ije, kmax, ntrc, delt, &
           kct, kcb,                   &
           cbmfx, cbmfxt, cape,        &
           hc, wca,                    &
           cmu, csu, cqu, ccu,         & 
           t, tv, tvm, s, q,           &
           z, dz, rdz, dzm )
!{{{ 
! inout
      real(DP),intent(inout) :: &
           cbmfx(ijs:ije),      &
           cape(ijs:ije) 
! input 
      integer,intent(in) ::      &
           kmax, ijs, ije, ntrc, &
           kct(ijs:ije),         &
           kcb(ijs:ije)
      real(DP),intent(in) :: &
           cbmfxt(ijs:ije),  &
           hc(ijs:ije),      & 
           wca(ijs:ije)
      real(DP),intent(in) ::    &
           cmu(ijs:ije,kmax),   &
           csu(ijs:ije,kmax),   &
           cqu(ijs:ije,kmax),   &
           ccu(ijs:ije,kmax),   &
           t(ijs:ije,kmax),     &
           tv(ijs:ije,kmax),    &
           tvm(ijs:ije,kmax+1), & 
           s(ijs:ije,kmax),     & 
           q(ijs:ije,kmax,ntrc),& 
           z(ijs:ije,kmax),     & 
           dz(ijs:ije,kmax),    & 
           rdz(ijs:ije,kmax),   & 
           dzm(ijs:ije,kmax+1), & 
           delt  
! work  
      real(DP),dimension(ijs:ije) :: &
           dcape, pcape, pcape_pbl, tauc
      real(DP) :: dum1, &
         sup, tup, qup, lup, tvup, aa, bb, &
         buoy, amp, taua, taua_min, taua_max
      integer :: ij, k

      !taua_min = max(720.0_dp,delt)
      taua_min = max(taumin,delt)
      taua_max = 10800.0_dp

! calc CAPE for each cloud 
      cape(:) = 0.0_dp 
      do k = 1, kmax
         do ij = ijs, ije
            if ( k>=kcb(ij) .and. k<=kct(ij) ) then
               ! in-cloud buoyancy 
               sup = csu(ij,k)/cmu(ij,k)
               qup = cqu(ij,k)/cmu(ij,k)
               tup = cpb*(sup-grav*z(ij,k))
               lup = ccu(ij,k)/cmu(ij,k)
               tvup = tup*(1.0+epsvt*qup-lup)
               buoy = grav*(tvup-tv(ij,k))/tv(ij,k)
               cape(ij) = cape(ij) + max(buoy,0.0_dp)*dz(ij,k)
            end if 
         end do
      end do

! for diagnostic closure 
      pcape(:) = 0.0_dp 
      dcape(:) = 0.0_dp

      do k = 1, kmax
         do ij = ijs, ije
            if ( k>=kcb(ij) .and. k<=kct(ij) ) then
               ! in-cloud buoyancy 
               sup = csu(ij,k)/cmu(ij,k)
               qup = cqu(ij,k)/cmu(ij,k)
               tup = cpb*(sup-grav*z(ij,k))
               lup = ccu(ij,k)/cmu(ij,k)
               tvup = tup*(1.0_dp+epsvt*qup-lup)
               buoy = grav*(tvup-tv(ij,k))/tv(ij,k)
               ! density weighted cape 
               pcape(ij) = pcape(ij) &
                  +max(buoy,0.0_dp)*rdz(ij,k) 
               if ( use_upwnd ) then
                  dum1 = (tv(ij,k+1)-tv(ij,k))/dzm(ij,k+1)
               else
                  dum1 = (tvm(ij,k+1)-tvm(ij,k))/dz(ij,k) 
               end if
               dcape(ij) = dcape(ij) & 
                 + grav/tv(ij,k)*max(dum1+grav/cp,0.0_dp)*dz(ij,k)*cmu(ij,k)
            end if 
         end do 
      end do 

      do ij = ijs, ije
         pcape(ij) = max(pcape(ij),0.0_dp)
         dcape(ij) = max(dcape(ij),dcapem)
         tauc(ij) = hc(ij)**2/max(wca(ij),small)
      end do 

      if ( use_pbl ) then
         pcape_pbl = 0.0_dp
         do k = 1, kmax
            do ij = ijs, ije
               if ( k<kcb(ij).and.kct(ij)>kcb(ij) ) then
                  sup = csu(ij,k)/cmu(ij,k)
                  qup = cqu(ij,k)/cmu(ij,k)
                  tup = cpb*(sup-grav*z(ij,k))
                  tvup = tup*(1.0_dp+epsvt*qup)
                  buoy = grav*(tvup-tv(ij,k))/tv(ij,k)
                  pcape_pbl(ij) = pcape_pbl(ij) &
                       +max(buoy,0.0_dp)*rdz(ij,k) 
               end if
            end do
         end do
         do ij = ijs, ije
            !pcape_pbl(ij) = tauc(ij)*max(pcape_pbl(ij),0.0_dp)
            pcape_pbl(ij) = max(pcape_pbl(ij),0.0_dp)
         end do
      else
         pcape_pbl = 0.0_dp
      end if 

! update cloud base mass flux 
      do ij = ijs, ije
         if ( kct(ij) > kcb(ij) ) then
            taua = tauc(ij)*fwave 
            taua = min(max(taua,taua_min),taua_max)
            if ( taun > 0.0_dp ) taua = taun  
            amp  = max(pcape(ij)-pcape_pbl(ij),0.3_dp*pcape(ij)) &
                 /(taua*dcape(ij))
            cbmfx(ij) = amp*cbmfxt(ij) 
            cbmfx(ij) = max(cbmfx(ij),0.0_dp)
         else 
            cbmfx(ij) = 0.0_dp 
         end if
      end do
!}}}
      end subroutine cuconvn

!--------------------------------------------------------------------
!     total bulk cloud mass flux and etc.
!--------------------------------------------------------------------
      subroutine cuflxn(                            &
           ijs, ije, kmax, ncp, icp,                &
           cbmfx, cbmfxt,                           &
           cprd, cmu, csu, cqu, ccu, ciu, cuu, cvu, &
           cmud, csud, cqud, ccud, cuud, cvud,      &
           cmuent, cmudet,                          &
           tprd, mu, msu, mqu, mcu, miu, muu, mvu,  &
           mud, msud, mqud, mcud, muud, mvud,       &
           muent, mudet )
!{{{ 
! inout 
      real(DP),intent(inout) :: & 
           cbmfx(ijs:ije,ncp),  & 
           mu(ijs:ije,kmax),    &     
           tprd(ijs:ije,kmax),  &    
           msu(ijs:ije,kmax),   &
           mqu(ijs:ije,kmax),   &
           mcu(ijs:ije,kmax),   &
           miu(ijs:ije,kmax),   &
           muu(ijs:ije,kmax),   &
           mvu(ijs:ije,kmax),   &
           mud(ijs:ije,kmax),   &
           msud(ijs:ije,kmax),  &
           mqud(ijs:ije,kmax),  &
           mcud(ijs:ije,kmax),  &
           muud(ijs:ije,kmax),  &
           mvud(ijs:ije,kmax),  &
           muent(ijs:ije,kmax), &
           mudet(ijs:ije,kmax)
! input 
      real(DP),intent(in) ::   &
           cbmfxt(ijs:ije),    &     
           cprd(ijs:ije,kmax), &   
           ccu(ijs:ije,kmax),  &   
           ciu(ijs:ije,kmax),  &   
           cmu(ijs:ije,kmax),  &
           csu(ijs:ije,kmax),  &
           cqu(ijs:ije,kmax),  &
           cuu(ijs:ije,kmax),  & 
           cvu(ijs:ije,kmax),  &
           cmud(ijs:ije,kmax), &
           csud(ijs:ije,kmax), &
           cqud(ijs:ije,kmax), &
           ccud(ijs:ije,kmax), &
           cuud(ijs:ije,kmax), &
           cvud(ijs:ije,kmax), &
           cmuent(ijs:ije,kmax), &
           cmudet(ijs:ije,kmax)
      integer,intent(in) :: & 
           kmax, ijs, ije, ncp, icp
! work  
      integer :: ij, k
      real(DP) :: fx(ijs:ije)

      if ( icp == 1 ) then
         mu = 0.0_dp 
         msu = 0.0_dp
         mqu = 0.0_dp
         mcu = 0.0_dp
         miu = 0.0_dp
         muu = 0.0_dp
         mvu = 0.0_dp

         mud = 0.0_dp
         msud = 0.0_dp
         mqud = 0.0_dp
         mcud = 0.0_dp
         muud = 0.0_dp
         mvud = 0.0_dp

         muent = 0.0_dp
         mudet = 0.0_dp

         tprd = 0.0_dp
      end if 

      do ij = ijs, ije
         fx(ij) = cbmfx(ij,icp)/cbmfxt(ij)
      end do 
 
      do k = 1, kmax 
         do ij = ijs, ije 
            ! in-cloud values
            mu (ij,k) = mu (ij,k) + cmu(ij,k)*fx(ij) 
            msu(ij,k) = msu(ij,k) + csu(ij,k)*fx(ij) 
            mqu(ij,k) = mqu(ij,k) + cqu(ij,k)*fx(ij) 
            mcu(ij,k) = mcu(ij,k) + ccu(ij,k)*fx(ij) 
            miu(ij,k) = miu(ij,k) + ciu(ij,k)*fx(ij) 
            muu(ij,k) = muu(ij,k) + cuu(ij,k)*fx(ij) 
            mvu(ij,k) = mvu(ij,k) + cvu(ij,k)*fx(ij) 
            ! precipi production 
            tprd(ij,k) = tprd(ij,k) + cprd(ij,k)*fx(ij) 
            ! detrianment 
            mud (ij,k) = mud (ij,k) + cmud(ij,k)*fx(ij)
            msud(ij,k) = msud(ij,k) + csud(ij,k)*fx(ij)
            mqud(ij,k) = mqud(ij,k) + cqud(ij,k)*fx(ij)
            mcud(ij,k) = mcud(ij,k) + ccud(ij,k)*fx(ij)
            muud(ij,k) = muud(ij,k) + cuud(ij,k)*fx(ij)
            mvud(ij,k) = mvud(ij,k) + cvud(ij,k)*fx(ij)
            ! entrainment/detrainment
            muent(ij,k) = muent(ij,k) + cmuent(ij,k)*fx(ij)
            mudet(ij,k) = mudet(ij,k) + cmudet(ij,k)*fx(ij)
         end do
      end do

! extract in-cloud values 
      if ( icp == ncp ) then
         do k = 1, kmax
            do ij = ijs, ije
               if ( mu(ij,k)>0.0_dp ) then
                  msu(ij,k) = msu(ij,k)/mu(ij,k) 
                  mqu(ij,k) = mqu(ij,k)/mu(ij,k) 
                  mcu(ij,k) = mcu(ij,k)/mu(ij,k) 
                  miu(ij,k) = miu(ij,k)/mu(ij,k) 
                  muu(ij,k) = muu(ij,k)/mu(ij,k) 
                  mvu(ij,k) = mvu(ij,k)/mu(ij,k) 
               end if
            end do
         end do
      end if
!}}}
      end subroutine cuflxn

!--------------------------------------------------------------------
!     updraft mass flux limiter
!--------------------------------------------------------------------
      subroutine culimup(                  &
           ijs, ije, kmax, ncp, kcb, delt, &
           dz, rdz,                        & 
           mu, mu_v, mud, mud_v,           &
           msud, mqud, mcud, muud, mvud,   &
           tprd, dbmfx )
!{{{
! inout 
        real(DP),intent(inout) :: &
             dbmfx(ijs:ije),      &
             mu(ijs:ije,kmax),    &
             mu_v(ijs:ije,kmax),  &
             mud(ijs:ije,kmax),   &
             mud_v(ijs:ije,kmax), &
             msud(ijs:ije,kmax),  &
             mqud(ijs:ije,kmax),  &
             mcud(ijs:ije,kmax),  &
             muud(ijs:ije,kmax),  &
             mvud(ijs:ije,kmax),  &
             tprd(ijs:ije,kmax)
! input 
        integer,intent(in) :: &
             ijs, ije, kmax, ncp, kcb(ijs:ije) 
        real(DP),intent(in) :: &
             delt,             &
             dz(ijs:ije,kmax), &
             rdz(ijs:ije,kmax)
! work 
        real(DP) :: fx2(ijs:ije,kmax)
        real(DP) :: fx2v(ijs:ije,kmax)
        real(DP) :: deltb, fxt, fxtv, afk, abk, bmfx, bmfxv
        integer :: ij, k, ii 

        deltb = 1.0_dp/delt 
        fx2 (:,:) = 1.0_dp
        fx2v(:,:) = 1.0_dp
        mu_v(:,:) = mu(:,:)
        mud_v(:,:) = mud(:,:)

        do k = 1, kmax
           do ij = ijs, ije
              bmfx  = fxcfl*rdz(ij,k)*deltb 
              bmfxv = fxcflv*rdz(ij,k)*deltb 
              if ( mu(ij,k) > bmfx ) then
                 fx2(ij,k) = min(fx2(ij,k),bmfx/mu(ij,k)) 
              end if
              if ( mu_v(ij,k) > bmfxv ) then
                 fx2v(ij,k) = min(fx2v(ij,k),bmfxv/mu_v(ij,k))
              end if
           end do
        end do
        
        do k = 1, kmax
           do ij = ijs, ije
              fxt = fx2(ij,k) 
              mu  (ij,k) = mu  (ij,k)*fxt
              mud (ij,k) = mud (ij,k)*fxt
              msud(ij,k) = msud(ij,k)*fxt
              mqud(ij,k) = mqud(ij,k)*fxt
              mcud(ij,k) = mcud(ij,k)*fxt
              tprd(ij,k) = tprd(ij,k)*fxt
              fxtv = fx2v(ij,k)
              mu_v (ij,k) = mu_v (ij,k)*fxtv
              mud_v(ij,k) = mud_v(ij,k)*fxtv
              muud (ij,k) = muud (ij,k)*fxtv
              mvud (ij,k) = mvud (ij,k)*fxtv
           end do
        end do

        do ij = ijs, ije
           k = kcb(ij)
           afk = dz(ij,k)/(dz(ij,k)+dz(ij,k-1))
           abk = 1.0_dp - afk
           dbmfx(ij) = mu(ij,k)*abk + mu(ij,k-1)*afk
        end do

!}}}
      end subroutine culimup

!--------------------------------------------------------------------
!     cloud water/ice partition
!--------------------------------------------------------------------
      subroutine cupart(    &           
           ijs, ije, kmax,  &
           t, mu, msu,      &
           mcu, mcud, tprd, & 
           mlu, miu, mlud, miud, stprd ) 
!{{{
! inout 
        real(DP),intent(inout) :: &
             mlu(ijs:ije,kmax),   &
             miu(ijs:ije,kmax),   &
             stprd(ijs:ije,kmax), &
             mlud(ijs:ije,kmax),  &
             miud(ijs:ije,kmax)
! input 
        integer,intent(in) :: &
             ijs, ije, kmax
        real(DP),intent(in) ::  &
             t(ijs:ije,kmax),   &
             mu(ijs:ije,kmax),  &
             msu(ijs:ije,kmax), &
             mcu(ijs:ije,kmax), &
             tprd(ijs:ije,kmax),&
             mcud(ijs:ije,kmax)

        integer :: ij, k 
        real(DP) :: fliq(ijs:ije) 
        real(DP) :: fice(ijs:ije)

        !----------------------
        ! liquid/ice partition
        !----------------------
        do k = 1, kmax
           do ij = ijs, ije
              ! ice fraction
              if ( use_ice ) then
                 fice(ij) = min(miu(ij,k)/max(mcu(ij,k),small),1.0_dp)
              else
                 fice(ij) = frcice(t(ij,k)) 
              end if
              fliq(ij) = 1.0_dp-fice(ij) 
           end do 
           do ij = ijs, ije
              if ( mu(ij,k) > 0.0_dp ) then
                 ! cloud condensate
                 miu(ij,k) = mcu(ij,k)*fice(ij)
                 mlu(ij,k) = mcu(ij,k)*fliq(ij)

                 ! detrainment
                 miud(ij,k) = mcud(ij,k)*fice(ij)
                 mlud(ij,k) = mcud(ij,k)*fliq(ij)
                 
                 ! precipitation generation flux 
                 stprd(ij,k) = tprd(ij,k)*fice(ij)
              else
                 miu(ij,k) = 0.0_dp
                 mlu(ij,k) = 0.0_dp
                 miud(ij,k) = 0.0_dp
                 mlud(ij,k) = 0.0_dp
                 stprd(ij,k) = 0.0_dp
              end if
           end do
        end do
!}}}
      end subroutine cupart

!--------------------------------------------------------------------
!     downdraft entrainment/detrainment
!     (precipi is treated same as in stratiform cloud) 
!--------------------------------------------------------------------
      subroutine cudscn(                        &    
           ijs, ije, kmax, ntrc, kcb, delt,     &
           tt, qt,                              &
           prcp, snwp, prcc, snwc,              & 
           mu, md, mdd, msdd, mqdd, mudd, mvdd, & 
           tprd, stprd,                         &
           s, q, u, v,                          &
           qs, hs, h, t, tv, p, pm, ro,         &
           msu, mqu, muu, mvu,                  &
           z, zm, dqs,                          &
           dz, rdz, rdzb, rhlim, dbmfx )
!{{{ 
! inout 
      real(DP),intent(inout) :: &
           tt(ijs:ije,kmax),    &   
           qt(ijs:ije,kmax,ntrc)
! output 
      real(DP),intent(out) ::  & 
           prcp(ijs:ije,kmax), & 
           snwp(ijs:ije,kmax), & 
           prcc(ijs:ije),      &
           snwc(ijs:ije),      &
           md(ijs:ije,kmax),   &
           mdd(ijs:ije,kmax),  &
           msdd(ijs:ije,kmax), &
           mqdd(ijs:ije,kmax), &
           mudd(ijs:ije,kmax), &
           mvdd(ijs:ije,kmax)
! input 
      integer,intent(in) :: & 
           kcb(ijs:ije),    &
           ijs, ije, ntrc,  &
           kmax 
      real(DP),intent(in) ::    &
           mu (ijs:ije,kmax),   &
           msu(ijs:ije,kmax),   &
           mqu(ijs:ije,kmax),   &
           muu(ijs:ije,kmax),   &
           mvu(ijs:ije,kmax),   &
           tprd (ijs:ije,kmax), &
           stprd(ijs:ije,kmax), &
           dz  (ijs:ije,kmax),  &
           rdz (ijs:ije,kmax),  &
           rdzb(ijs:ije,kmax),  &
           hs (ijs:ije,kmax),   &
           h  (ijs:ije,kmax),   &
           qs (ijs:ije,kmax),   &
           dqs(ijs:ije,kmax)
      real(DP),intent(in) ::     &
           delt,                 &
           s (ijs:ije,kmax),     &   
           q (ijs:ije,kmax,ntrc),& 
           t (ijs:ije,kmax),     &
           tv(ijs:ije,kmax),     &
           p (ijs:ije,kmax),     &
           pm(ijs:ije,kmax+1),   &
           ro(ijs:ije,kmax),     & 
           u (ijs:ije,kmax),     &
           v (ijs:ije,kmax),     &
           z (ijs:ije,kmax),     &
           zm(ijs:ije,kmax+1),   &
           dbmfx(ijs:ije),       &
           rhlim(ijs:ije) 
! work 
      integer :: ij, k, &
           klfs(ijs:ije)
      real(DP) ::  &
         entrt, detrt,  & 
         fice, fliq, buoy, &
         qsat, tdown, tvdown
      real(DP) :: qtest, ttest, utest, vtest, &
         gam, twb, qwb, sup, qup, tup, &
         deltb, sdown, qdown, ccnd, &
         entdz, detdz, escld, edown, &
         dum1, dum2, dum3, tnd_prc, tnd_snw, taum, &
         rqrain, smlt
      real(DP) :: buoya(ijs:ije)
      real(DP) :: hmmin(ijs:ije) 
      real(DP) :: prfl(ijs:ije) 
      real(DP) :: dprfl(ijs:ije,kmax) 
      real(DP) :: hst(ijs:ije,kmax) 
      real(DP) :: msd(ijs:ije,kmax) 
      real(DP) :: mqd(ijs:ije,kmax) 
      real(DP) :: mud(ijs:ije,kmax) 
      real(DP) :: mvd(ijs:ije,kmax) 
      integer :: khmin(ijs:ije) 
      integer :: kctm(ijs:ije) 
      logical :: lddraf(ijs:ije) 
      logical :: ldevp(ijs:ije) 
      logical :: ldbas(ijs:ije) 
      logical :: lddet 
      logical :: llo(ijs:ije) 

      deltb = 1.0_dp/delt 

      prcp(:,:) = 0.0_dp
      snwp(:,:) = 0.0_dp      

      md (:,:) = 0.0_dp
      mdd(:,:) = 0.0_dp

      msd = 0.0_dp
      mqd = 0.0_dp
      mud = 0.0_dp
      mvd = 0.0_dp

      buoya(:) = 0.0_dp 

      prcc(:) = 0.0_dp
      snwc(:) = 0.0_dp
      prfl(:) = 0.0_dp
      dprfl(:,:) = 0.0_dp

      msdd(:,:) = 0.0_dp 
      mqdd(:,:) = 0.0_dp 
      mudd(:,:) = 0.0_dp 
      mvdd(:,:) = 0.0_dp 

      ! find maximum height of minimum mse. 
      khmin(:) = kmax
      hmmin(:) = 1.0e10_dp
      if ( kh_type == 1 ) then
        hst = hs ! saturated mse 
      else
        hst = h ! normal mse 
      end if 
      do k = 2, kmax
         do ij = ijs, ije 
            if ( hst(ij,k)<hmmin(ij) .and. hst(ij,k)<hst(ij,k-1) ) then
               hmmin(ij) = hst(ij,k) 
               khmin(ij) = k 
            end if 
         end do 
      end do

      ! bulk cloud top index
      kctm(:) = 0 
      do k = 1, kmax
         do ij = ijs, ije
            if ( mu(ij,k)>0.0_dp ) then
               kctm(ij) = max(kctm(ij),k)
            end if
         end do
      end do

      ! find level of free sinking (LFS)
      lddraf(:) = .false.
      klfs  (:) = 1
      prfl  (:) = 0.0_dp 
      do k = kmax, 1, -1
         do ij = ijs, ije
            ! precipi increase 
            prfl(ij) = prfl(ij) + tprd(ij,k) 
            ! calc LFS or not
            llo(ij) = k>=kcb(ij).and.k<=kctm(ij).and.k<=khmin(ij) &
               .and.(.not.lddraf(ij)).and.prfl(ij)>0.0_dp
         end do 
         do ij = ijs, ije
            if ( llo(ij) ) then
               qup = mqu(ij,k)
               sup = msu(ij,k)
               tup = (sup-grav*z(ij,k))*cpb
               gam = felt(t(ij,k))*cpb*dqs(ij,k) 
               ccnd = (qs(ij,k)-q(ij,k,1))/(1.0_dp+gam)
               ccnd = max(ccnd,0.0_dp) 
               twb = t(ij,k) - felt(t(ij,k))*cpb*ccnd
               qwb = q(ij,k,1) + ccnd
               ttest = 0.5_dp*(tup+twb)
               qtest = 0.5_dp*(qup+qwb)
               utest = 0.5_dp*(u(ij,k)+muu(ij,k))
               vtest = 0.5_dp*(v(ij,k)+mvu(ij,k))
               buoy = ttest*(1.0_dp+epsvt*qtest) &
                 -t(ij,k)*(1.0_dp+epsvt*q(ij,k,1))
               edown = max(qwb-q(ij,k,1),0.0_dp) &
                 *eta_dn*dbmfx(ij) 
               if ( buoy<0.0_dp .and. prfl(ij)>th_dn*edown ) then
                  klfs(ij) = k 
                  md (ij,k) = eta_dn*dbmfx(ij)
                  msd(ij,k) = (cp*ttest+grav*z(ij,k))*md(ij,k)
                  mqd(ij,k) = qtest*md(ij,k)
                  mud(ij,k) = utest*md(ij,k)
                  mvd(ij,k) = vtest*md(ij,k)
                  lddraf(ij) = .true.
                  dprfl(ij,k) = 0.5_dp*edown
                  prfl(ij) = prfl(ij)-dprfl(ij,k)
               end if
            end if
         end do
      end do

      ! start downdraft calculation
      kloop : do k = kmax-1, 1, -1

         ! downdraft mass flux detrain height 
         do ij = ijs, ije
            if ( dnbas_type == 1 ) then
               ldbas(ij) = zm(ij,k+1)-zm(ij,1) <= 500.0_dp
            else
               ldbas(ij) = pm(ij,1)-pm(ij,k+1) <= 60.0e2_dp
            end if
         end do

         !-------------------
         ! downdraft budget 
         !-------------------
         do ij = ijs, ije
            if ( k < klfs(ij) .and. lddraf(ij) ) then

               md (ij,k) = md (ij,k+1)
               msd(ij,k) = msd(ij,k+1)
               mqd(ij,k) = mqd(ij,k+1)
               mud(ij,k) = mud(ij,k+1)
               mvd(ij,k) = mvd(ij,k+1)

               ! precipi increase
               prfl(ij) = prfl(ij) + tprd(ij,k) 

               ! dse correction 
               tdown = (msd(ij,k)/md(ij,k)-grav*z(ij,k))*cpb
               tdown = min(max(tdown,tmin),tmax) 
               msd(ij,k) = (cp*tdown+grav*z(ij,k))*md(ij,k) 

               ! downdraft evaporation 
               qsat = fqsat(tdown,p(ij,k))
               gam = felt(tdown)*cpb*fdqsat(tdown,qsat) 
 
               edown = (qsat*md(ij,k)-mqd(ij,k))/(1.0_dp+gam)
               edown = max(min(edown,prfl(ij)),0.0_dp)

               msd(ij,k) = msd(ij,k) - edown*felt(tdown)
               mqd(ij,k) = mqd(ij,k) + edown 
 
               ! precipi decrease by evap/subl
               dprfl(ij,k) = edown 
               prfl(ij) = prfl(ij)-dprfl(ij,k)

               ! in-cloud buoyancy 
               sdown = msd(ij,k)/md(ij,k) 
               qdown = mqd(ij,k)/md(ij,k) 
               tdown = cpb*(sdown-grav*z(ij,k))
               tvdown = tdown*(1.0_dp+epsvt*qdown)
               buoy = grav*(tvdown-tv(ij,k))/tv(ij,k)

               ! downdraft entrainment rate
               buoya(ij) = buoya(ij) + min(buoy,0.0_dp)*dz(ij,k)
               if ( use_entorg ) then
                  entrt = max(-adown*buoy/(wlfs**2-buoya(ij)),0.0_dp)
                  entrt = min(entrt,dentmax)
               else
                  entrt = 0.0_dp 
               end if

               if ( ddet_type == 1 ) then
                 lddet = buoy > 0.0_dp 
               else
                 lddet = buoy > 0.0_dp .or. &
                      prfl(ij)<=0.0_dp 
               end if 

               ! downdraft detrainment rate
               if ( lddet ) then
                  detrt = 1.0_dp/dz(ij,k)
                  if ( k == kcb(ij) ) &
                  detrt = 1.0_dp/(zm(ij,k+1)-zm(ij,1))
               else
                  detrt = 0.0_dp 
               end if

               ! turbulent ent/det
               entrt = entrt + dturb
               detrt = detrt + dturb
               detrt = min(detrt,1.0_dp/dz(ij,k))

               ! linear detrain near the ground surface 
               if ( ldbas(ij) ) then
                  entrt = 0.0_dp 
                  detrt = 1.0_dp/(zm(ij,k+1)-zm(ij,1))
                  !detrt = max(detrt,1.0_dp/(zm(ij,k+1)-zm(ij,1)))
               end if

               if ( md(ij,k) > fxcfl*rdz(ij,k)*deltb ) then
                  entrt = 0.0_dp 
               end if 
               
               ! downdraft budget (entrain)
               entdz = entrt*dz(ij,k)*md(ij,k) 
               md (ij,k) = md (ij,k) + entdz 
               msd(ij,k) = msd(ij,k) + entdz*s(ij,k) 
               mqd(ij,k) = mqd(ij,k) + entdz*q(ij,k,1) 
               mud(ij,k) = mud(ij,k) + entdz*u(ij,k) 
               mvd(ij,k) = mvd(ij,k) + entdz*v(ij,k) 

               ! downdraft budget (detrain)
               detdz = detrt*dz(ij,k) 
               mdd(ij,k) = md(ij,k)*detdz 
               md(ij,k) = max(md(ij,k)-mdd(ij,k),0.0_dp) 

               if ( md(ij,k)>0.0_dp ) then
                  msdd(ij,k) = msd(ij,k)*detdz 
                  mqdd(ij,k) = mqd(ij,k)*detdz 
                  mudd(ij,k) = mud(ij,k)*detdz
                  mvdd(ij,k) = mvd(ij,k)*detdz
                  msd(ij,k) = msd(ij,k) - msdd(ij,k)
                  mqd(ij,k) = mqd(ij,k) - mqdd(ij,k)
                  mud(ij,k) = mud(ij,k) - mudd(ij,k)
                  mvd(ij,k) = mvd(ij,k) - mvdd(ij,k) 
               else
                  lddraf(ij) = .false.
                  msdd(ij,k) = msd(ij,k)
                  mqdd(ij,k) = mqd(ij,k)
                  mudd(ij,k) = mud(ij,k)
                  mvdd(ij,k) = mvd(ij,k)
                  msd(ij,k) = 0.0_dp
                  mqd(ij,k) = 0.0_dp
                  mud(ij,k) = 0.0_dp
                  mvd(ij,k) = 0.0_dp
               end if

            end if
         end do

      end do kloop

      ! start calc of tendency to env. 
      prfl = 0.0_dp
      prcc(:) = 0.0_dp
      snwc(:) = 0.0_dp
      do k = kmax, 1, -1
         do ij = ijs, ije
            ! update precipi by in-cloud tendency 
            fice = snwc(ij)/max(prfl(ij),small)
            fice = min(max(fice,0.0_dp),1.0_dp)

            prfl(ij) = prfl(ij)+tprd(ij,k)-dprfl(ij,k) 
            snwc(ij) = snwc(ij)+stprd(ij,k)-fice*dprfl(ij,k)  
            prfl(ij) = max(prfl(ij),0.0_dp)
            snwc(ij) = max(snwc(ij),0.0_dp)

            ! snow melting 
            if ( mlt_type == 1 ) then
              taum = 11800.0_dp/(1.0_dp+0.5_dp*max(t(ij,k)-tmelt,0.0_dp))
            else
              taum = delt 
            end if 
            smlt = max(t(ij,k)-tmelt,0.0_dp)*rdz(ij,k)/(elfm*taum)
            smlt = min(smlt,snwc(ij)) 
            snwc(ij) = snwc(ij) - smlt
            tt(ij,k) = tt(ij,k) - elfm*smlt*rdzb(ij,k)

            ! evap/subl in subcloud layer
            if ( k < kcb(ij) ) then
               qsat = fqsat(t(ij,k),p(ij,k))
               rqrain = prfl(ij) &
                 /(5.11e-3_dp*cconv)*sqrt(p(ij,k)/pm(ij,1)) 
               escld = cconv*5.44e-4_dp &
                    *max(rhlim(ij)*qsat-q(ij,k,1),0.0_dp) &
                    *rqrain**0.5777_dp
               escld = escld*rdz(ij,k) ! kg/m2/s
               escld = min(escld,prfl(ij)) 
 
               fice = snwc(ij)/max(prfl(ij),small)
               fice = min(max(fice,0.0_dp),1.0_dp)

               prfl(ij) = max(prfl(ij)-escld,0.0_dp)
               snwc(ij) = max(snwc(ij)-fice*escld,0.0_dp) 
               qt(ij,k,1) = qt(ij,k,1) + escld*rdzb(ij,k) 
               tt(ij,k) = tt(ij,k) - cpb*felt(t(ij,k))*escld*rdzb(ij,k)
            end if 

            !prcp(ij,k) = prcc(ij)
            prcp(ij,k) = max(prfl(ij)-snwc(ij),0.0_dp)
            snwp(ij,k) = snwc(ij)
         end do 
      end do 

      ! rain flux at the ground surface
      do ij = ijs, ije
        prcc(ij) = max(prfl(ij)-snwc(ij),0.0_dp) 
      end do 
!}}}
      end subroutine cudscn

!--------------------------------------------------------------------
!     up/down detrainment
!--------------------------------------------------------------------
      subroutine cudetall(                     &
           ijs, ije, kmax, ntrc, kcb, delt,    &
           tt, qt, ut, vt,                     &
           dbmfx, md, mdd, md_v, mdd_v,        &
           msdd, mqdd, mudd, mvdd,             &
           mud, mud_v,                         &
           msud, mqud, mlud, miud, muud, mvud, &
           psu, pqu, puu, pvu,                 &
           s, q, u, v, rob,                    &
           zm, dz, rdz, rdzb ) 
!{{{
! inout 
        real(DP),intent(inout) ::   &
             tt(ijs:ije,kmax),      &
             qt(ijs:ije,kmax,ntrc), &
             ut(ijs:ije,kmax),      &
             vt(ijs:ije,kmax),      &
             msdd(ijs:ije,kmax),    &
             mqdd(ijs:ije,kmax),    &
             mudd(ijs:ije,kmax),    &
             mvdd(ijs:ije,kmax),    &
             md(ijs:ije,kmax),      &
             mdd(ijs:ije,kmax)
! output 
        real(DP),intent(out) ::  &
             md_v(ijs:ije,kmax), &
             mdd_v(ijs:ije,kmax)
! input 
        integer,intent(in) :: &
             ijs, ije, kmax, ntrc, &
             kcb(ijs:ije) 
        real(DP),intent(in) ::     &
             delt,                 &
             rdzb(ijs:ije,kmax),   &
             rdz(ijs:ije,kmax),    &
             zm(ijs:ije,kmax+1),   &
             dz(ijs:ije,kmax),     &
             s(ijs:ije,kmax),      &
             q(ijs:ije,kmax,ntrc), &
             u(ijs:ije,kmax),      &
             v(ijs:ije,kmax),      &
             rob(ijs:ije,kmax)
        real(DP),intent(in) ::    &
             dbmfx(ijs:ije),      &
             mud  (ijs:ije,kmax), & 
             mud_v(ijs:ije,kmax), & 
             msud (ijs:ije,kmax), & 
             mqud (ijs:ije,kmax), & 
             mlud (ijs:ije,kmax), & 
             miud (ijs:ije,kmax), & 
             muud (ijs:ije,kmax), & 
             mvud (ijs:ije,kmax), &
             psu  (ijs:ije,kmax), &
             pqu  (ijs:ije,kmax), &
             puu  (ijs:ije,kmax), &
             pvu  (ijs:ije,kmax)
        
        integer :: ij, k
        real(DP) :: fxv(ijs:ije,kmax)
        real(DP) :: bmfx, bmfxv, deltb, zbase, afk, abk, &
             sci, qvci, qlci, qici, uci, vci, fxtv 
        real(DP) :: scb(ijs:ije) 
        real(DP) :: qcb(ijs:ije) 
        real(DP) :: ucb(ijs:ije) 
        real(DP) :: vcb(ijs:ije) 
        
        deltb = 1.0_dp/delt 

        md_v (:,:) = md (:,:)
        mdd_v(:,:) = mdd(:,:)

        ! mass flux limiter
        fxv(:,:) = 1.0_dp
        do k = 1, kmax
           do ij = ijs, ije
              bmfxv = fxcflv*rdz(ij,k)*deltb 
              if ( md_v(ij,k) > bmfxv ) then
                 fxv(ij,k) = min(fxv(ij,k),bmfxv/md_v(ij,k)) 
              end if
           end do
        end do

        do k = 1, kmax
           do ij = ijs, ije
              fxtv = fxv(ij,k)
              md_v (ij,k) = md_v (ij,k)*fxtv 
              mdd_v(ij,k) = mdd_v(ij,k)*fxtv 
              mudd(ij,k) = mudd(ij,k)*fxtv 
              mvdd(ij,k) = mvdd(ij,k)*fxtv
           end do
        end do

        ! updraft/downdraft detrainment 
        do k = 1, kmax
           do ij = ijs, ije

              sci = rdzb(ij,k)*( &
                    msud(ij,k)+msdd(ij,k) &
                   -(mud(ij,k)+mdd(ij,k))*s(ij,k) )

              qvci = rdzb(ij,k)*( &
                     mqud(ij,k)+mqdd(ij,k) &
                    -(mud(ij,k)+mdd(ij,k))*q(ij,k,1) )

              uci = rdzb(ij,k)*( &
                     muud(ij,k)+mudd(ij,k) &
                    -(mud_v(ij,k)+mdd_v(ij,k))*u(ij,k) )
              vci = rdzb(ij,k)*( &
                     mvud(ij,k)+mvdd(ij,k) &
                    -(mud_v(ij,k)+mdd_v(ij,k))*v(ij,k) )

              if ( use_env ) then
                qlci = rdzb(ij,k)*( & 
                       mlud(ij,k) &
                     -(mud(ij,k)+mdd(ij,k))*q(ij,k,itl) )
                qici = rdzb(ij,k)*( & 
                       miud(ij,k) &
                     -(mud(ij,k)+mdd(ij,k))*q(ij,k,iti) )
              else
                qlci = rdzb(ij,k)*mlud(ij,k) 
                qici = rdzb(ij,k)*miud(ij,k) 
              end if 
              
              tt(ij,k) = tt(ij,k) + sci*cpb
              qt(ij,k,1  ) = qt(ij,k,1  ) + qvci
              qt(ij,k,itl) = qt(ij,k,itl) + qlci
              qt(ij,k,iti) = qt(ij,k,iti) + qici
              ut(ij,k) = ut(ij,k) + uci
              vt(ij,k) = vt(ij,k) + vci
              
           end do
        end do

        ! subcloud layer tendency (TK89 type)
        if ( base_type == 1 ) then
           do ij = ijs, ije
              k = kcb(ij) 
              afk = dz(ij,k)/(dz(ij,k)+dz(ij,k-1))
              abk = 1.0_dp - afk 
              scb(ij) = psu(ij,k)*abk + psu(ij,k-1)*afk
              qcb(ij) = pqu(ij,k)*abk + pqu(ij,k-1)*afk
              ucb(ij) = puu(ij,k)*abk + puu(ij,k-1)*afk
              vcb(ij) = pvu(ij,k)*abk + pvu(ij,k-1)*afk
           end do 
           do k = 1, kmax
              do ij = ijs, ije
                 if ( k < kcb(ij) ) then
                    zbase = zm(ij,kcb(ij))-zm(ij,1)
                    fxtv = -rob(ij,k)*dbmfx(ij)/zbase
                    sci  = fxtv*(scb(ij)-s(ij,k))
                    qvci = fxtv*(qcb(ij)-q(ij,k,1  ))
                    qlci = fxtv*(0.0_dp-q(ij,k,itl))
                    qici = fxtv*(0.0_dp-q(ij,k,iti))
                    uci  = fxtv*(ucb(ij)-u(ij,k))
                    vci  = fxtv*(vcb(ij)-v(ij,k))
                    tt(ij,k) = tt(ij,k) + sci*cpb
                    qt(ij,k,1  ) = qt(ij,k,1  ) + qvci
                    qt(ij,k,itl) = qt(ij,k,itl) + qlci
                    qt(ij,k,iti) = qt(ij,k,iti) + qici
                    ut(ij,k) = ut(ij,k) + uci
                    vt(ij,k) = vt(ij,k) + vci
                 end if
              end do
           end do
        end if 
!}}}        
      end subroutine cudetall

!--------------------------------------------------------------------
!     compensative subsidence (implicit)
!--------------------------------------------------------------------
      subroutine cusbsdn(              &
           ijs, ije, kmax, ntrc, delt, &
           tt, qt, ut, vt,             &
           s, q, u, v, t,              &
           rob, dzm,                   &
           mu, md, mu_v, md_v )
!{{{
! inout 
      real(DP),intent(inout) ::  &
           tt(ijs:ije,kmax),     &
           qt(ijs:ije,kmax,ntrc),&
           ut(ijs:ije,kmax),     &
           vt(ijs:ije,kmax),     &
           mu(ijs:ije,kmax),     &
           md(ijs:ije,kmax),     &
           mu_v(ijs:ije,kmax),   &
           md_v(ijs:ije,kmax)
! input
      real(DP),intent(in) ::    &
           delt,                &
           s(ijs:ije,kmax),     &
           q(ijs:ije,kmax,ntrc),&
           u(ijs:ije,kmax),     &
           v(ijs:ije,kmax),     &
           t(ijs:ije,kmax),     &
           rob(ijs:ije,kmax),   &
           dzm(ijs:ije,kmax+1)
      integer,intent(in) :: &
           ijs, ije, kmax, ntrc
! work
      integer :: ij, k 
      real(DP) :: wvec(ijs:ije,kmax)
      real(DP) :: sn(ijs:ije,kmax)
      real(DP) :: tn(ijs:ije,kmax)
      real(DP) :: qn(ijs:ije,kmax,ntrc)
      real(DP) :: un(ijs:ije,kmax)
      real(DP) :: vn(ijs:ije,kmax)
      real(DP) :: deltb
      real(DP),dimension(ijs:ije,kmax) :: &
         ssrc, usrc, vsrc, tsrc
      real(DP) :: qsrc(ijs:ije,kmax,ntrc)

      do k = 1, kmax
         do ij = ijs, ije
           ssrc(ij,k)=cp*tt(ij,k)
           usrc(ij,k)=ut(ij,k)
           vsrc(ij,k)=vt(ij,k)
           qsrc(ij,k,1)=qt(ij,k,1)
           qsrc(ij,k,itl)=qt(ij,k,itl)
           qsrc(ij,k,iti)=qt(ij,k,iti)
         end do
      end do 

      do ij = ijs, ije
         mu  (ij,kmax) = 0.0_dp
         mu_v(ij,kmax) = 0.0_dp
         md  (ij,1) = 0.0_dp
         md_v(ij,1) = 0.0_dp
      end do

! heat & moisture 
      do k = 1, kmax
         do ij = ijs, ije
            wvec(ij,k) = (-mu(ij,k)+md(ij,k))*rob(ij,k)
         end do 
      end do 

      if ( use_temp ) then
         do k = 1, kmax
            do ij = ijs, ije
               tsrc(ij,k) = tt(ij,k) &
                    -wvec(ij,k)*grav*cpb
            end do
         end do
         call vtintg(ijs,ije,kmax,delt, &
              wvec,dzm,t,tn,tsrc) 
      else
         call vtintg(ijs,ije,kmax,delt, &
              wvec,dzm,s,sn,ssrc) 
      end if

      call vtintg(ijs,ije,kmax,delt, &
           wvec,dzm,q(:,:,1),qn(:,:,1),qsrc(:,:,1)) 

! cloud condensate 
      if ( .not. use_env ) then
         do k = 1, kmax
            do ij = ijs, ije
               wvec(ij,k) = -mu(ij,k)*rob(ij,k)
            end do
         end do
      end if 

      call vtintg(ijs,ije,kmax,delt, &
           wvec,dzm,q(:,:,itl),qn(:,:,itl),qsrc(:,:,itl)) 

      call vtintg(ijs,ije,kmax,delt, &
           wvec,dzm,q(:,:,iti),qn(:,:,iti),qsrc(:,:,iti)) 

! momentum 
      do k = 1, kmax
         do ij = ijs, ije
            wvec(ij,k) = (-mu_v(ij,k)+md_v(ij,k))*rob(ij,k)
         end do 
      end do 

      call vtintg(ijs,ije,kmax,delt, &
           wvec,dzm,u,un,usrc) 

      call vtintg(ijs,ije,kmax,delt, &
           wvec,dzm,v,vn,vsrc) 

! final tendencies 
      deltb = 1.0_dp/delt 
      do k = 1, kmax
         do ij = ijs, ije
            qn(ij,k,1  ) = max(qn(ij,k,1  ),0.0_dp)
            qn(ij,k,itl) = max(qn(ij,k,itl),0.0_dp)
            qn(ij,k,iti) = max(qn(ij,k,iti),0.0_dp)
            qt(ij,k,1  ) = (qn(ij,k,1  )-q(ij,k,1  ))*deltb
            qt(ij,k,itl) = (qn(ij,k,itl)-q(ij,k,itl))*deltb
            qt(ij,k,iti) = (qn(ij,k,iti)-q(ij,k,iti))*deltb
            if ( use_temp ) then
               tt(ij,k) = (tn(ij,k)-t(ij,k))*deltb
            else
               tt(ij,k) = (sn(ij,k)-s(ij,k))*deltb*cpb
            end if
            ut(ij,k) = (un(ij,k)-u(ij,k))*deltb 
            vt(ij,k) = (vn(ij,k)-v(ij,k))*deltb 
         end do
      end do
!}}}
      end subroutine cusbsdn

!--------------------------------------------------------------------
!     TDMA solver for cusbsdn 
!--------------------------------------------------------------------
      subroutine vtintg(         &
           ijs, ije, kmax, delt, &
           w, dzm, phi, phin, src )
!{{{
! output 
        real(DP),intent(out) :: phin(ijs:ije,kmax)
! input
        integer,intent(in) :: ijs, ije, kmax
        real(DP),intent(in) :: delt 
        real(DP),intent(in) :: w(ijs:ije,kmax)
        real(DP),intent(in) :: phi(ijs:ije,kmax)
        real(DP),intent(in) :: src(ijs:ije,kmax)
        real(DP),intent(in) :: dzm(ijs:ije,kmax+1)
! work 
        real(DP) :: q(ijs:ije,kmax)
        real(DP) :: p(ijs:ije,kmax)

        integer :: k, ij 
        real(DP) :: aa, bb, cc, dd, den

        ! a*x(i) = b*x(i+1) + c*x(i-1) + d 
        do k = 1, kmax
           do ij = ijs, ije
              cc = 0.5_dp*(+w(ij,k)+abs(w(ij,k)))/dzm(ij,k  ) 
              bb = 0.5_dp*(-w(ij,k)+abs(w(ij,k)))/dzm(ij,k+1) 
              aa = 0.5_dp*(+w(ij,k)+abs(w(ij,k)))/dzm(ij,k  ) &
                 + 0.5_dp*(-w(ij,k)+abs(w(ij,k)))/dzm(ij,k+1) &
                 + 1.0_dp/delt 
              dd = phi(ij,k)/delt+src(ij,k)

              if ( k == 1 ) then
                 p(ij,k) = bb/aa
                 q(ij,k) = dd/aa
              else
                 den = aa - cc*p(ij,k-1) 
                 p(ij,k) = bb/den 
                 q(ij,k) = (dd+cc*q(ij,k-1))/den 
              end if
           end do
        end do

        do ij = ijs, ije
           phin(ij,kmax) = q(ij,kmax) 
        end do

        do k = kmax-1, 1, -1
           do ij = ijs, ije
              phin(ij,k) = p(ij,k)*phin(ij,k+1) + q(ij,k) 
           end do
        end do
!}}}
      end subroutine vtintg 

!--------------------------------------------------------------------
!     divided compensative subsidence (implicit)
!--------------------------------------------------------------------
      subroutine cusbsdn_div(    &
           ijs, ije, kmax, ntrc, &
           delt,                 &
           tt, qt, ut, vt,       &
           s, q, u, v, t,        &
           ro, rob, dzm,         &
           mu, md, mu_v, md_v )
!{{{
! inout 
      real(DP),intent(inout) ::  &
           tt(ijs:ije,kmax),     &
           qt(ijs:ije,kmax,ntrc),&
           ut(ijs:ije,kmax),     &
           vt(ijs:ije,kmax),     &
           mu(ijs:ije,kmax),     &
           md(ijs:ije,kmax),     &
           mu_v(ijs:ije,kmax),   &
           md_v(ijs:ije,kmax)
! input
      real(DP),intent(in) ::    &
           delt,                &
           s(ijs:ije,kmax),     &
           q(ijs:ije,kmax,ntrc),&
           u(ijs:ije,kmax),     &
           v(ijs:ije,kmax),     &
           t(ijs:ije,kmax),     &
           ro(ijs:ije,kmax),    &
           rob(ijs:ije,kmax),   &
           dzm(ijs:ije,kmax+1)
      integer,intent(in) :: &
           ijs, ije, kmax, ntrc
! work
      integer :: ij, k 
      real(DP) :: wu(ijs:ije,kmax)
      real(DP) :: wd(ijs:ije,kmax)
      real(DP) :: sn(ijs:ije,kmax)
      real(DP) :: tn(ijs:ije,kmax)
      real(DP) :: qvn(ijs:ije,kmax)
      real(DP) :: qln(ijs:ije,kmax)
      real(DP) :: qin(ijs:ije,kmax)
      real(DP) :: un(ijs:ije,kmax)
      real(DP) :: vn(ijs:ije,kmax)
      real(DP) :: deltb
      real(DP),dimension(ijs:ije,kmax) :: &
         ssrc, usrc, vsrc, tsrc, &
         qvsrc, qlsrc, qisrc
      real(DP) :: qvt, qlt, qit, qvt0, qlt0, qit0 

      do k = 1, kmax
         do ij = ijs, ije
           ssrc (ij,k) = cp*tt(ij,k)
           usrc (ij,k) = ut(ij,k)
           vsrc (ij,k) = vt(ij,k)
           qvsrc(ij,k) = qt(ij,k,1  )
           qlsrc(ij,k) = qt(ij,k,itl)
           qisrc(ij,k) = qt(ij,k,iti)
         end do
      end do 

      do ij = ijs, ije
         mu  (ij,kmax) = 0.0_dp
         mu_v(ij,kmax) = 0.0_dp
         md  (ij,1   ) = 0.0_dp
         md_v(ij,1   ) = 0.0_dp
      end do

! heat & moisture 
      do k = 1, kmax
         do ij = ijs, ije
            wu(ij,k) = -mu(ij,k)
            wd(ij,k) = +md(ij,k)
         end do 
      end do 

      call vtintg_div( ijs, ije, kmax, delt, &
                       wu, wd, dzm, ro, s, sn, ssrc ) 

      call vtintg_div( ijs, ije, kmax, delt, &
                       wu, wd, dzm, ro, q(:,:,1), qvn, qvsrc ) 

! cloud condensate 
      if ( .not. use_env ) then
         do k = 1, kmax
            do ij = ijs, ije
               wu(ij,k) = -mu(ij,k)
               wd(ij,k) = 0.0_dp
            end do
         end do
      end if 

      call vtintg_div( ijs, ije, kmax, delt, &
                       wu, wd, dzm, ro, q(:,:,itl), qln, qlsrc ) 

      call vtintg_div( ijs, ije, kmax, delt, &
                       wu, wd, dzm, ro, q(:,:,iti), qin, qisrc ) 

! momentum 
      do k = 1, kmax
         do ij = ijs, ije
            wu(ij,k) = -mu_v(ij,k)
            wd(ij,k) = +md_v(ij,k)
         end do 
      end do 

      call vtintg_div( ijs, ije, kmax, delt, &
                       wu, wd, dzm, ro, u, un, usrc ) 

      call vtintg_div( ijs, ije, kmax, delt, &
                       wu, wd, dzm, ro, v, vn, vsrc ) 

! final tendencies 
      deltb = 1.0_dp/delt
      do k = 1, kmax
         do ij = ijs, ije
            qvt0 = (qvn(ij,k)-q(ij,k,1  ))*deltb
            qlt0 = (qln(ij,k)-q(ij,k,itl))*deltb
            qit0 = (qin(ij,k)-q(ij,k,iti))*deltb
            qlt = max(qlt0,-q(ij,k,itl)*deltb)
            qit = max(qit0,-q(ij,k,iti)*deltb)
            qvt = qvt0-(qlt-qlt0)-(qit-qit0)
            qt(ij,k,1  ) = qvt 
            qt(ij,k,itl) = qlt 
            qt(ij,k,iti) = qit 
            tt(ij,k) = cpb*(sn(ij,k)-s(ij,k))*deltb
            tt(ij,k) = tt(ij,k) &
                     + cpb*felt(t(ij,k))*((qlt-qlt0)+(qit-qit0))
            ut(ij,k) = (un(ij,k)-u(ij,k))*deltb 
            vt(ij,k) = (vn(ij,k)-v(ij,k))*deltb 
         end do
      end do
!}}}
      end subroutine cusbsdn_div

!--------------------------------------------------------------------
!     TDMA solver for cusbsdn_div
!--------------------------------------------------------------------
      subroutine vtintg_div(     &
           ijs, ije, kmax, delt, &
           wu, wd, dzm, ro, phi, phin, src )
!{{{
! output 
        real(DP),intent(out) :: phin(ijs:ije,kmax)
! input
        integer,intent(in) :: ijs, ije, kmax
        real(DP),intent(in) :: delt
        real(DP),intent(in) :: wu(ijs:ije,kmax)
        real(DP),intent(in) :: wd(ijs:ije,kmax)
        real(DP),intent(in) :: ro(ijs:ije,kmax)
        real(DP),intent(in) :: phi(ijs:ije,kmax)
        real(DP),intent(in) :: src(ijs:ije,kmax)
        real(DP),intent(in) :: dzm(ijs:ije,kmax+1)
! work 
        real(DP) :: q(ijs:ije,kmax)
        real(DP) :: p(ijs:ije,kmax)

        integer :: k, ij 
        real(DP) :: aa, bb, cc, dd, den, resi

        ! a*x(i) = b*x(i+1) + c*x(i-1) + d 
        do k = 1, kmax
           do ij = ijs, ije
              if ( sbint_type == 1 ) then
                 cc = +wd(ij,k)/dzm(ij,k  ) 
                 bb = -wu(ij,k)/dzm(ij,k+1) 
                 aa = +wd(ij,k)/dzm(ij,k  ) &
                      -wu(ij,k)/dzm(ij,k+1) & 
                      +ro(ij,k)/delt 
                 dd = +ro(ij,k)*phi(ij,k)/delt &
                      +ro(ij,k)*src(ij,k)
              else
                 cc = +0.5_dp*wd(ij,k)/dzm(ij,k  ) 
                 bb = -0.5_dp*wu(ij,k)/dzm(ij,k+1)
                 aa = +0.5_dp*wd(ij,k)/dzm(ij,k  ) &
                      -0.5_dp*wu(ij,k)/dzm(ij,k+1) &
                      +ro(ij,k)/delt                  
                 dd = +ro(ij,k)*phi(ij,k)/delt &
                      +ro(ij,k)*src(ij,k) 
                 if ( k == 1 ) then
                    resi = -0.5_dp*wu(ij,k)*(-phi(ij,k)+phi(ij,k+1))/dzm(ij,k+1)
                 else if ( k == kmax ) then
                    resi = -0.5_dp*wd(ij,k)*(-phi(ij,k-1)+phi(ij,k))/dzm(ij,k  ) 
                 else
                    resi = -0.5_dp*wd(ij,k)*(-phi(ij,k-1)+phi(ij,k))/dzm(ij,k  ) &
                           -0.5_dp*wu(ij,k)*(-phi(ij,k)+phi(ij,k+1))/dzm(ij,k+1)
                 end if
                 dd = dd + resi 
              end if

              if ( k == 1 ) then
                 p(ij,k) = bb/aa
                 q(ij,k) = dd/aa
              else
                 den = aa - cc*p(ij,k-1) 
                 p(ij,k) = bb/den 
                 q(ij,k) = (dd+cc*q(ij,k-1))/den 
              end if
           end do
        end do

        do ij = ijs, ije
           phin(ij,kmax) = q(ij,kmax) 
        end do

        do k = kmax-1, 1, -1
           do ij = ijs, ije
              phin(ij,k) = p(ij,k)*phin(ij,k+1) + q(ij,k) 
           end do
        end do
!}}}
      end subroutine vtintg_div

!--------------------------------------------------------------------
!     ice fraction function 
!--------------------------------------------------------------------
      real(DP) function frcice(t)
!{{{
        real(DP),intent(in) :: t

        frcice = min( max( (timax-t)/(timax-timin), 0.0_dp ), 1.0_dp )
!}}}
      end function frcice

!--------------------------------------------------------------------
!     precipi function 
!--------------------------------------------------------------------
      real(DP) function fprc(l,llim,w,t)
!{{{
        real(DP),intent(in) :: l, llim, w, t
        real(DP) :: cbf, cprf, lcritf
        real(DP),parameter :: lcrit = 0.5e-3_dp
        real(DP),parameter :: tbf = 268.0_dp

        if ( t < tbf ) then 
           cbf = 1.0_dp + 0.5_dp*sqrt(min(tbf-t,tbf-timin))
        else
           cbf = 1.0_dp 
        end if 

        cprf   = cpr0*cbf
        lcritf = lcrit/cbf

        if ( l > llim ) then
          fprc = cprf/w*max(1.0_dp-exp(-(l/lcritf)**2),0.0_dp)
        else
          fprc = 0.0_dp 
        end if 
!}}}
      end function fprc

  !============================================================
      real(DP) function felt(t) ! latent heat
        real(DP),intent(in) :: t

        felt = elel+emelt/2.0_dp*(1.0_dp-sign(1.0_dp,t-tmelt))

      end function felt

  !============================================================
      real(DP) function fqsat(t,p) ! saturation mixing ratio 
        real(DP),intent(in) :: t, p

        fqsat  = epsv * es0 / p &
             * exp( (elel+emelt/2.0_dp*(1.0_dp-sign(1.0_dp,t-tmelt))) &
             /rvap *( 1.0_dp/tmelt - 1.0_dp/t )           )

      end function fqsat

  !============================================================
      real(DP) function fdqsat(t,qs) ! dqsat/dT
        real(DP),intent(in) :: t, qs

        fdqsat = (elel+emelt/2.0_dp*(1.0_dp-sign(1.0_dp,t-tmelt))) &
             * qs / ( rvap * t*t )

      end function fdqsat

#endif 

!--------------------------------------------------------------------
!     parameter setting 
!--------------------------------------------------------------------
      subroutine cumcnv__prep(mrank,P_namelist,ncp,dphi)
!{{{
      integer,intent(in) :: &
        mrank, &   ! rank of node 
        ncp, &     ! number of cloud type
        P_namelist ! output number of namelist file 
      real(DP),intent(in) :: &
        dphi       ! Increment of latitude  

#ifdef use_cumcnv_

      integer,parameter :: OK = 0, NG = 1, EOF = -1
      integer :: ios, icp
      real(DP) :: delwub, twave

      ! for cloud base
      rewind(P_namelist)
      read (P_namelist, nml=nmcnvcb, iostat=ios )
      if ( ios >= ng ) then
         call fatal_msg('failure in reading nmcnvcb namelist.')
      end if 
#ifdef altix
      if ( mrank == 0 ) then
#endif
      write(6,nml=nmcnvcb) 
#ifdef altix
      end if 
#endif

      ! for updraft 
      rewind(P_namelist) 
      read (P_namelist, nml=nmcnvup, iostat=ios )
      if ( ios >= ng ) then
         call fatal_msg('failure in reading nmcnvup namelist.')
      end if 
#ifdef altix
      if ( mrank == 0 ) then
#endif
      write(6,nml=nmcnvup) 
#ifdef altix
      end if 
#endif

      ! for cloud base mass flux
      rewind (P_namelist)
      read ( P_namelist, nml=nmcnvmf, iostat=ios )
      if ( ios >= ng ) then
         call fatal_msg('failure in reading nmcnvmf namelist.')
      end if 

      ! coeff. of convective adjustment closure
      twave = 2.0_dp*pi/(dphi*3.0_dp)
      fwave = (1.0_dp+264.0_dp/(1.5_dp*twave))

#ifdef altix
      if ( mrank == 0 ) then
#endif
      write(6,nml=nmcnvmf) 
#ifdef altix
      end if 
#endif

      ! for downdraft 
      rewind (P_namelist) 
      read ( P_namelist, nml=nmcnvdn, iostat=ios )
      if ( ios >= ng ) then
         call fatal_msg('failure in reading nmcnvdn namelist.')
      end if 
#ifdef altix
      if ( mrank == 0 ) then
#endif
      write(6,nml=nmcnvdn) 
#ifdef altix
      end if 
#endif

      ! for new scheme parameter
      rewind (P_namelist)
      read ( P_namelist, nml=nmcnvnew, iostat=ios )
      if ( ios >= ng ) then
         call fatal_msg('failure in reading nmcnvnew namelist.')
      end if 
#ifdef altix
      if ( mrank == 0 ) then
#endif
      write(6,nml=nmcnvnew) 
#ifdef altix
      end if 
#endif

      ! for compensative subsidence 
      rewind (P_namelist)
      read ( P_namelist, nml=nmcnvsb, iostat=ios )
      if ( ios >= ng ) then
         call fatal_msg('failure in reading nmcnvsb namelist.')
      end if 
#ifdef altix
      if ( mrank == 0 ) then
#endif
      write(6,nml=nmcnvsb) 
#ifdef altix
      end if 
#endif

      ! up/down entrainment rate max/min
      uentmax = max( uentmax, 0.0_dp )
      dentmax = max( dentmax, 0.0_dp )

      ! set updraft velocity for each cloud type 
      allocate( wub0(ncp) ) 
      delwub = (wumax-wumin)/real(ncp)
      do icp = 1, ncp 
         wub0(icp) = ( real(icp)*delwub )**2
      end do 

#endif 
!}}}
      end subroutine cumcnv__prep 

!--------------------------------------------------------------------
!     make fatal message & stop program 
!--------------------------------------------------------------------
      subroutine fatal_msg( msg )
!{{{
        use mpi 
        character(len=*),intent(in) :: msg 

        integer :: errcode = 999 
        integer :: ierr

        print *,'!!!'//msg//'!!!'

        call mpi_abort(mpi_comm_world,errcode,ierr) 
!}}}
      end subroutine fatal_msg

 end module atmos__cumcnv

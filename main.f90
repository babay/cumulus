 program main

   use constants 
   use atmos__cumcnv

   integer :: ierr, my_rank 

   integer :: P_namelist = 5

   integer :: ncp = 14
   real(DP) :: dphi = 1.0_dp

   call mpi_init(ierr) 
   call mpi_comm_rank(mpi_comm_world,my_rank,ierr) 

!===================================================

! set cumulus parameters
   call cumcnv__prep(my_rank,P_namelist,ncp,dphi) 

! call main cumulus routine 
!  call cumcnv__main 

!===================================================

   call mpi_finalize(ierr) 

   stop 
 end program main

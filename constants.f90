 module constants

 use mpi

 integer,parameter :: DP = kind(1.d0) 

 real(DP),parameter :: pi = 3.141592653589793238462643383279502884_dp

 real(DP),parameter :: rair    = 287.04_dp  ! gas constant for air
 real(DP),parameter :: cpair   = 1004.6_dp  ! specific heat at constant pressure 
 real(DP),parameter :: rvap    = 461.0_dp   ! gas constant for vapor 
 real(DP),parameter :: epsv    = rair/rvap  ! gas constant ratio 
 real(DP),parameter :: epsvt   = 1.0_dp/epsv-1.0_dp ! 1/epsilon-1

 real(DP),parameter :: g_accel = 9.80665_dp ! gravitational acceleration 
 real(DP),parameter :: ps00    = 1.0e5_dp   ! reference pressure 

 real(DP),parameter :: tmelt   = 273.15_dp  ! melting temp (K) 
 real(DP),parameter :: emelt   = 3.40e5_dp  ! latent heat of fusion
 real(DP),parameter :: elel    = 2.5e6_dp   ! latent heat of evap
 real(DP),parameter :: es0     = 611.0_dp   ! saturate e 0deg 

 end module constants
